//
//  PaymentDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/10/21.
//

import Foundation



struct PaymentDetails{
     var paymentId : String = ""
     var paymentMode : PaymentMode = .COD
     var paymentAmount : Int = 0
     var paymentReferanceNumber : String = ""
     var paymentStatus : String = ""
     var bankName : String  = ""
   
}

enum PaymentMode : String {
    case COD = "Cash On Delivery"
    case DebitCard = "Debit Card"
    case CreditCard = "Credit Card"
    case UPI = "UPI"
    case Netbanking = "Netbanking"
    case Wallets = "Wallet"
}
