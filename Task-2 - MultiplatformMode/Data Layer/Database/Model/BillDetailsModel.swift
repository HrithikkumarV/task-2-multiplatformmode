//
//  BillDetailsModel.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 17/03/22.
//

import Foundation

struct BillDetailsModel{
     var orderId  : String = ""
     var itemTotal : Int = 0
     var deliveryFee : Int = 0
     var restaurantGST : Double = 0.0
     var restaurantPackagingCharges : Int = 0
     var itemDiscount : Int = 0
     var totalCost : Int = 0
    
}
