//
//  MenuDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/10/21.
//

import Foundation


struct MenuDetails{
    
     var restaurantId : Int = 0
     var menuName : String = ""
     var menuDescription : String = ""
     var menuPrice : Int = 0
     var menuCategoryId : Int = 0
     var menuTarianType : String = ""
     var menuImage : Data? = nil
   
}

