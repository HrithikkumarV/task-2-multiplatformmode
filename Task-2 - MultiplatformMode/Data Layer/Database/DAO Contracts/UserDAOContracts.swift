//
//  DataBase  Contracts.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 27/10/21.
//

import Foundation


protocol UserAccountsDAOContracts : UserLoginDAOContract ,  UserCreateAccountDAOContract , UserAddressDAOContract , LocalPersistUserIdDAOContract,LocalPersistedUserAddressDAOContract,UserAccountsPageDAOContract{
}

protocol UserLoginDAOContract {
    func checkIfUserPhoneNumberExists(phoneNumber : String) throws -> Bool
    func getUserId(phoneNumber : String) throws -> Int
}

protocol UserCreateAccountDAOContract{
    func persistUserAccountCreateAccountDetails(user : UserDetails) throws -> Bool
    func checkIfUserPhoneNumberExists(phoneNumber : String) throws -> Bool
    func getUserId(phoneNumber : String) throws -> Int
    
}

protocol UserAddressDAOContract {
    func PersistUserAddress(userId : Int,userAddressDetails  : UserAddressDetails) throws -> Bool
    func getUserAddresses(userId  :Int) throws -> [UserAddressDetails]
    func updateUserAddress(userAddressDetails  : UserAddressDetails) throws -> Bool
    func getUserAddress(userAddressId: String) throws -> UserAddressDetails
    func deleteUserAddress(userAddressId :String) throws -> Bool
}

protocol LocalPersistUserIdDAOContract {
    func persistUserId(userId : Int) throws -> Bool
    func deleteUserId() throws -> Bool
    func getUserID() throws -> Int
}

protocol LocalPersistedUserAddressDAOContract{
    func getLocalPersistedUserAddress() throws -> UserAddressDetails
    func deleteLocalPersistedUserAddress() throws -> Bool
    func PersistLocalPersistedUserAddress(userAddressDetails  : UserAddressDetails) throws -> Bool
    func updateUserAddressIdInLocalPersistedUserAddress(userAddressId : String) throws -> Bool
    func updateLocalPersistedAddress(userAddressDetails  : UserAddressDetails) throws -> Bool
}

protocol UserWelcomePageDAOContract{
    func getUserDetails(userId : Int) throws ->  UserDetails
}

protocol UserAccountsPageDAOContract{
    func getUserDetails(userId : Int) throws ->  UserDetails
    func updateUserName(userId : Int, userName :String) throws -> Bool
    func updateUserPhoneNumber(userId : Int, phoneNumber :String) throws -> Bool
    func updateUserDetails(userId : Int, userDetails : UserDetails) throws -> Bool
    
}

protocol RestaurantsDAOContracts : RestaurantsTabDAOContract{
}

protocol RestaurantsTabDAOContract{
    func getRestaurentsTabContentDetails(locality : String, pincode : String) throws -> [RestaurantCompleteDetails]
}

protocol UserDisplayMenuDetailsPageDAOContract{
    func getMenuDetailsofRestaurantForUser(restaurantId: Int) throws -> [MenuContentDetails]
    func getCategoryDetails(restaurantId : Int) throws -> [(categoryId : Int , categoryName : String)]
}

protocol FavouriteRestaurantDAOContract{
    func persistFavouriteRestaurant(userId: Int,restaurantId : Int) throws -> Bool
    func deleteFavouriteRestaurant(favouriteRestaurantId : Int) throws -> Bool
    func getFavouriteRestaurantIds(userId : Int) throws -> [Int : Int]
    func getFavouriteRestaurants(userId : Int) throws -> [RestaurantCompleteDetails]
}

protocol SearchMenuAndRestaurantDAOContract{
    func getMenusForSearchedItem(searchItem : String, locality : String , pincode  :String) throws -> [(menu : MenuContentDetails,restaurant : RestaurantCompleteDetails)]
    
    func getRestaurantsForSearchedItem(searchItem : String, locality : String , pincode : String) throws -> [RestaurantCompleteDetails]
    func persistSearchHistory(searchItemName : String ,searchItemType : SearchItemType) throws -> Bool
    
    
    func clearSearchHistory() throws -> Bool
    
    func getSearchHistoryItemsLikeTheSearchItem(searchItem :String) throws -> [(searchItemName : String , searchItemType : SearchItemType)]
    
    func getSearchHistoryItems() throws -> [(searchItemName : String , searchItemType : SearchItemType)]
    
    func clearSearchHistoryItem(searchHistoryId : Int) throws -> Bool
    
    func getMenusForSearchedItemSuggestion(searchItem: String, locality: String , pincode : String) throws -> Set<String>
    
    func getRestaurantsForSearchedItemSuggestion(searchItem: String, locality: String , pincode : String) throws -> Set<String>
}

protocol UserCartDAOContract{
    func getCartMenuItems() throws -> [Int : Int]
    func insertMenuToCart(cartDetails : (menuId : Int , restaurantId : Int , quantity : Int)) throws -> Bool
    func updateMenuInCart(menuId : Int , quantity : Int) throws -> Bool 
    func removeMenuFromCart(menuId : Int) throws -> Bool
    func clearCart() throws -> Bool
    func getRestaurantNameFromUserCart() throws -> String
    func getMenuPrice(menuId: Int) throws -> Int
    func getRestaurantDetailsFromUserCart() throws -> RestaurantCompleteDetails
    func getMenuDetailsInCart() throws -> [CartMenuDetails]
}

protocol CouponDAOContract{
    func persistCouponCode(couponCode : String) throws -> Bool
    func deleteCouponCode() throws -> Bool
    func getCouponCode() throws -> String
}




protocol UserOrdersDAOContract : FoodRatingContract{
    func persistOrderDetails(orderDetails: OrderModel) throws -> Bool
    
    func persistTrackOrderDetails(orderStatus : OrderStatus , orderId : String) throws -> Bool
    
    func persistPaymentDetails(paymentDetails : PaymentDetails) throws -> Bool
    
    func persistInstructionToRestaurantDetails(instructionToRestaurant  : String , orderId : String) throws -> Bool
    
    func persistMenuToOrderedFoodDetails(orderedFoodDetails: (menuId: Int, orderId: String, quantity: Int)) throws -> Bool
    
    func getActiveOrderDetails(userId : Int)throws -> [OrderDetails]
    
    func getpastOrderDetails(userId : Int , offSet : Int , limit : Int) throws -> [OrderDetails]
    
    func getMenuDetailsInActiveOrderFoodDetails(userId : Int) throws -> [ String: [OrderMenuDetails]]
    
    func getMenuDetailsInpastOrderFoodDetails(orderIdListString : String) throws -> [ String: [OrderMenuDetails]]
    
    func updateOrderCancellationReason(orderId : String , orderCancellationReason : String) throws -> Bool
    
    func getOrderCancellationReason(orderId : String) throws -> String
    
    func persistBillDetails(billDetails : BillDetailsModel) throws -> Bool
    
    func getBillDetailsOfOrder(orderId : String) throws -> BillDetailsModel
    
    func updateOrderStatus(orderId : String, orderStatus : OrderStatus) throws -> Bool
    
    func getOrderStatus(orderId : String) throws -> OrderStatus
    
    func getRestaurantDetails(restaurantId: Int) throws -> RestaurantCompleteDetails 
}


protocol FoodRatingContract{
    func updateFoodRating(orderId : String , starRating : Int) throws -> Bool
    
    
    func updateRestaurantStarRating(starRating :Int , restaurantId : Int) throws -> Bool
    
    
    func updateFoodFeedback(orderId : String , feedback : String) throws -> Bool
    
    
    func createFoodRatingRow(orderId : String) throws -> Bool

}

