//
//  Common-DAOContracts.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 19/01/22.
//

import Foundation

protocol DatabaseServiceContract :CreateDatabaseTablesContract,UserAccountsDAOContracts,UserWelcomePageDAOContract,UserOrdersDAOContract,CouponDAOContract,UserCartDAOContract,SearchMenuAndRestaurantDAOContract,FavouriteRestaurantDAOContract,UserDisplayMenuDetailsPageDAOContract,RestaurantsDAOContracts,RestaurantAccountsDAOContracts,RestaurantOrdersDAOContract,RestaurantMenuDetailsDAOContract{
    
}

protocol CreateDatabaseTablesContract{
    func createTables() -> Bool
}


