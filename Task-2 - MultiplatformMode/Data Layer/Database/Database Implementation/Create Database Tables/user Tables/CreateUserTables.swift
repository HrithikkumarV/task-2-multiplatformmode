//
//  CreateDatabaseTables.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/03/22.
//

import Foundation
import SQLite3


extension DatabaseService {
    
    func initialiseCreateUserTables() throws{
        try createUserAccountsTable()
        try createUserAddressesTable()
        try createLocalPersistedPreviousUsedUserAddressTable()
        try createLocalPersistUserId()
    }
    
    private func createUserAccountsTable() throws {
        let query = "CREATE TABLE IF NOT EXISTS user_accounts(user_id INTEGER DEFAULT 0 PRIMARY KEY AUTOINCREMENT , user_name TEXT NOT NULL, user_phone_number TEXT NOT NULL UNIQUE ,user_last_signin TEXT NOT NULL, user_joined_at TEXT NOT NULL, user_account_status INTEGER DEFAULT 1 NOT NULL);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(queryStatement)
        
    }
    
    private func createUserAddressesTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS user_addresses(pk_user_address_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT,user_address_id TEXT NOT NULL UNIQUE,user_id INTEGER NOT NULL,user_address_tag TEXT NOT NULL ,user_door_number_and_building_name_and_building_number TEXT NOT NULL, user_street_name TEXT NOT NULL, user_locality TEXT NOT NULL,user_landmark TEXT NOT NULL, user_city TEXT NOT NULL, user_state TEXT NOT NULL,user_country TEXT NOT NULL , user_pincode TEXT NOT NULL,user_address_status INREGER NOT NULL DEFAULT 1,FOREIGN KEY (user_id) REFERENCES user_accounts (user_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(queryStatement)
        
    }
    
    private func createLocalPersistUserId() throws{
        let query = "CREATE TABLE IF NOT EXISTS local_persist_user_Id(local_id INTEGER DEFAULT 0 PRIMARY KEY , user_id INTEGER NOT NULL );"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createLocalPersistedPreviousUsedUserAddressTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS local_persisted_previous_used_user_address(previous_used_user_address_id INTEGER DEFAULT 1 PRIMARY KEY, user_address_id TEXT NOT NULL, user_address_tag  TEXT NOT NULL ,user_door_number_and_building_name_and_building_number TEXT NOT NULL, user_street_name TEXT NOT NULL, user_locality TEXT NOT NULL,user_landmark TEXT NOT NULL, user_city TEXT NOT NULL, user_state TEXT NOT NULL,user_country TEXT NOT NULL , user_pincode TEXT NOT NULL);"
        
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
        sqlite3_finalize(queryStatement)
        
    }
}
