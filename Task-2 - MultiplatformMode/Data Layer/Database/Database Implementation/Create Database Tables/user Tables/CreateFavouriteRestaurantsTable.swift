//
//  CreateFavouriteRestaurantsTable.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/03/22.
//

import Foundation
import SQLite3
extension DatabaseService {
    
    func initialiseCreateFavouriteRestaurantsTable() throws{
        try createFavouriteRestaurantTable()
    }
    
    private func createFavouriteRestaurantTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS favourite_restaurant(favourite_restaurant_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , user_id INTEGER NOT NULL , restaurant_id INTEGER NOT NULL,FOREIGN KEY (restaurant_id) REFERENCES restaurant_accounts (restaurant_id) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (user_id) REFERENCES user_accounts (user_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
}
