//
//  DatabaseTablesIntialisation.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/03/22.
//

import Foundation
import SQLite3

extension DatabaseService : CreateDatabaseTablesContract{
   
    
    func createTables() -> Bool{
       
        do{
           
            try initialiseCreateRestaurantsTables()
            try initialiseCreateUserTables()
            try initialiseCreateFavouriteRestaurantsTable()
            try initialiseCreateMenuTables()
            try initialiseCreateUserCartTable()
            try initialiseCreateCouponAppliedTable()
            try initialiseCreateOrdersTable()
            try initialiseCreateSearchHistoryTable()
         
           return true
        }
        catch {
            print(error)
        }
        return false
    }
    
    
    
}
