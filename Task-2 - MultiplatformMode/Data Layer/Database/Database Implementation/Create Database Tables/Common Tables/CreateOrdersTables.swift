//
//  CreateOrdersTables.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation
import SQLite3

extension DatabaseService {
    
    func initialiseCreateOrdersTable() throws{
        try createOrdersTable()
        try createOrderedFoodDetailsTable()
        try createUserInstructionToRestaurantTable()
        try createTrackOrderTable()
        try createPaymentDetailsTable()
        try createBillDetailsTable()
        try createFoodRatingTable()
    }
    
    
    private func createOrdersTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS orders(pk_order_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , order_id TEXT NOT NULL UNIQUE ,user_id INTEGER NOT NULL , restaurant_id INTEGER NOT NULL,user_address_id INTEGER NOT NULL,payment_id TEXT NOT NULL ,date_and_time TEXT NOT NULL,FOREIGN KEY (restaurant_id) REFERENCES restaurant_accounts (restaurant_id) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (user_id) REFERENCES user_accounts (user_id) ON UPDATE CASCADE ON DELETE CASCADE ,FOREIGN KEY (user_address_id) REFERENCES user_addresses(user_address_id) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (payment_id) REFERENCES payment_details(payment_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createOrderedFoodDetailsTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS ordered_food_details(ordered_food_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , menu_id INTEGER NOT NULL , order_id TEXT NOT NULL,quantity INTEGER NOT NULL,FOREIGN KEY (order_id) REFERENCES orders (order_id) ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (menu_id) REFERENCES menu_details (menu_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createUserInstructionToRestaurantTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS user_instruction_to_restaurant(instruction_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT ,user_instruction_to_restaurant  TEXT NOT NULL DEFAULT '' , order_id TEXT NOT NULL,FOREIGN KEY (order_id) REFERENCES orders (order_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createTrackOrderTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS track_order(track_order_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT ,order_status TEXT NOT NULL, order_id TEXT NOT NULL,order_cancellation_reason TEXT,FOREIGN KEY (order_id) REFERENCES orders (order_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createPaymentDetailsTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS payment_details(pk_payment_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT ,payment_id TEXT NOT NULL UNIQUE ,payment_mode TEXT NOT NULL,payment_amount INTEGER NOT NULL,payment_status TEXT,payment_referance_number TEXT,bank_name TEXT);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createBillDetailsTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS bill_details(bill_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT ,item_total INTERGER DEFAULT 0 NOT NULL, delivery_fee INTERGER DEFAULT 0 NOT NULL,restaurant_gst INTEGER DEFAULT 0 NOT NULL, restaurant_packaging_charges INTERGER DEFAULT 0 NOT NULL , discount_amount INTEGER DEFAULT 0 NOT NULL,total_cost INTEGER DEFAULT 0 NOT NULL ,order_id TEXT NOT NULL,FOREIGN KEY (order_id) REFERENCES orders (order_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
    
    private func createFoodRatingTable() throws{
        let query = "CREATE TABLE IF NOT EXISTS food_rating(rating_id INTEGER DEFAULT 1 PRIMARY KEY AUTOINCREMENT , star_rating INTERGER DEFAULT 0 NOT NULL,feedback TEXT  DEFAULT '',order_id TEXT NOT NULL,FOREIGN KEY (order_id) REFERENCES orders (order_id) ON UPDATE CASCADE ON DELETE CASCADE);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_step(queryStatement) == SQLITE_DONE else{
                throw SQLiteError.Step(message: errorMessage)
            }
            sqlite3_finalize(queryStatement)
    }
}
