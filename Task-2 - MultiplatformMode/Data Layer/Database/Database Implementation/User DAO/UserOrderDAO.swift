//
//  OrderDAO.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation
import SQLite3

extension DatabaseService : UserOrdersDAOContract{
    
    func persistOrderDetails(orderDetails: OrderModel) throws -> Bool {
        
        var isExecuted = false
        let insertQuery = "INSERT INTO orders(restaurant_id , user_id ,date_and_time,user_address_id,order_id,payment_id) VALUES (?,?,?,?,?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        print("Yess" ,orderDetails.orderId )
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(orderDetails.restaurantId)) == SQLITE_OK && sqlite3_bind_int(insertQueryStatement , 2, Int32(orderDetails.userId )) == SQLITE_OK &&
           
            sqlite3_bind_text(insertQueryStatement , 3, ( orderDetails.dateAndTime  as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 4, ( orderDetails.userAddressId as NSString).utf8String, -1, nil) == SQLITE_OK  && sqlite3_bind_text(insertQueryStatement , 5, ( orderDetails.orderId as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 6, ( orderDetails.paymentId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
        
    }
    
    func persistTrackOrderDetails(orderStatus : OrderStatus , orderId : String) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO track_order(order_status,order_id) VALUES (?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement , 1, ( orderStatus.rawValue as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
        
    }
    
    func persistPaymentDetails(paymentDetails : PaymentDetails) throws -> Bool {
        
        var isExecuted = false
        let insertQuery = "INSERT INTO payment_details(payment_id,payment_mode,payment_amount ,payment_status,payment_referance_number ,bank_name ) VALUES (?,?,?,?,?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        
        guard sqlite3_bind_text(insertQueryStatement , 1, ( paymentDetails.paymentId as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 2, ( paymentDetails.paymentMode.rawValue as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_int(insertQueryStatement , 3, Int32(paymentDetails.paymentAmount)) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 4, ( paymentDetails.paymentStatus as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 5, ( paymentDetails.paymentReferanceNumber as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 6, ( paymentDetails.bankName as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }
    
    func persistInstructionToRestaurantDetails(instructionToRestaurant  : String , orderId : String) throws -> Bool {
    
        var isExecuted = false
        let insertQuery = "INSERT INTO user_instruction_to_restaurant(user_instruction_to_restaurant,order_id) VALUES (?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement , 1, ( instructionToRestaurant as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        }else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
    }
    
    
    
    
    
    func persistMenuToOrderedFoodDetails(orderedFoodDetails: (menuId: Int, orderId: String, quantity: Int)) throws -> Bool {
        ()
        var isExecuted = false
        let insertQuery = "INSERT INTO ordered_food_details(menu_id,quantity,order_id) VALUES (?,?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        print(orderedFoodDetails.orderId , "Noo")
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(orderedFoodDetails.menuId)) == SQLITE_OK && sqlite3_bind_int(insertQueryStatement , 2, Int32(orderedFoodDetails.quantity)) == SQLITE_OK &&
            sqlite3_bind_text(insertQueryStatement , 3, ( orderedFoodDetails.orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
    }
    
    func getActiveOrderDetails(userId : Int)throws -> [OrderDetails]{
        ()
        var ordersDetails : [OrderDetails] = []
        let Query = "Select orders.order_id, user_id , orders.restaurant_id ,user_address_id ,date_and_time,order_status , payment_amount ,user_instruction_to_restaurant,payment_mode , restaurant_name FROM orders INNER JOIN track_order ON track_order.order_id = orders.order_id INNER JOIN restaurant_accounts ON restaurant_accounts.restaurant_id = orders.restaurant_id INNER JOIN payment_details ON payment_details.payment_id = orders.payment_id INNER JOIN user_instruction_to_restaurant ON user_instruction_to_restaurant.order_id = orders.order_id  WHERE  user_id = ? and order_status != 'Delivered' and order_status != 'Cancelled' and order_status != 'Declined' ORDER BY pk_order_id DESC;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(userId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderDetails = OrderDetails()
            orderDetails.orderModel.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 0))
            orderDetails.orderModel.userId =  Int(sqlite3_column_int(QueryStatement, 1))
            orderDetails.orderModel.restaurantId =  Int(sqlite3_column_int(QueryStatement, 2))
            orderDetails.orderModel.userAddressId =  String(cString: sqlite3_column_text(QueryStatement, 3))
            orderDetails.orderModel.dateAndTime =  String(cString: sqlite3_column_text(QueryStatement, 4))
            orderDetails.orderStatus =  OrderStatus(rawValue: String(cString: sqlite3_column_text(QueryStatement, 5)))!
            orderDetails.orderTotal =  Int(sqlite3_column_int(QueryStatement, 6))
            orderDetails.instructionToRestaurant =  String(cString: sqlite3_column_text(QueryStatement, 7))
            orderDetails.paymentMode =  PaymentMode(rawValue: String(cString: sqlite3_column_text(QueryStatement, 8))) ?? .COD
            orderDetails.restaurantName =  String(cString: sqlite3_column_text(QueryStatement, 9))
            ordersDetails.append(orderDetails)
        }
        sqlite3_finalize(QueryStatement)
        ()
        return ordersDetails
    }
    
    
    
    func getpastOrderDetails(userId : Int , offSet : Int , limit : Int) throws -> [OrderDetails]{
        ()
        var ordersDetails : [OrderDetails] = []
        let Query = "Select orders.order_id, user_id , orders.restaurant_id ,user_address_id ,date_and_time,order_status , payment_amount ,user_instruction_to_restaurant , payment_mode ,restaurant_name , star_rating , feedback FROM orders INNER JOIN track_order ON track_order.order_id = orders.order_id INNER JOIN restaurant_accounts ON restaurant_accounts.restaurant_id = orders.restaurant_id INNER JOIN payment_details ON payment_details.payment_id = orders.payment_id INNER JOIN user_instruction_to_restaurant ON  user_instruction_to_restaurant.order_id = orders.order_id INNER JOIN food_rating ON food_rating.order_id = orders.order_id  WHERE  user_id = ? and (order_status = 'Delivered' or order_status = 'Cancelled' or order_status = 'Declined') ORDER BY pk_order_id DESC LIMIT ? OFFSET ? ;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(userId)) == SQLITE_OK &&
                sqlite3_bind_int(QueryStatement , 2, Int32(limit)) == SQLITE_OK &&
                sqlite3_bind_int(QueryStatement , 3, Int32(offSet)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderDetails = OrderDetails()
            orderDetails.orderModel.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 0))
            orderDetails.orderModel.userId =  Int(sqlite3_column_int(QueryStatement, 1))
            orderDetails.orderModel.restaurantId =  Int(sqlite3_column_int(QueryStatement, 2))
            orderDetails.orderModel.userAddressId =  String(cString: sqlite3_column_text(QueryStatement, 3))
            orderDetails.orderModel.dateAndTime =  String(cString: sqlite3_column_text(QueryStatement, 4))
            orderDetails.orderStatus =  OrderStatus(rawValue: String(cString: sqlite3_column_text(QueryStatement, 5)))!
            orderDetails.orderTotal =  Int(sqlite3_column_int(QueryStatement, 6))
            orderDetails.instructionToRestaurant =  String(cString: sqlite3_column_text(QueryStatement, 7))
            orderDetails.paymentMode =  PaymentMode(rawValue: String(cString: sqlite3_column_text(QueryStatement, 8))) ?? .COD
            orderDetails.restaurantName =  String(cString: sqlite3_column_text(QueryStatement, 9))
            orderDetails.starRating =   Int(sqlite3_column_int(QueryStatement, 10))
            orderDetails.feedback =  String(cString: sqlite3_column_text(QueryStatement, 11))
            ordersDetails.append(orderDetails)
        }
        sqlite3_finalize(QueryStatement)
        ()
        return ordersDetails
    }
    
    func getMenuDetailsInActiveOrderFoodDetails(userId : Int) throws -> [ String: [OrderMenuDetails]]{
        ()
        var menuDetailsInOrderedFoods: [String :[OrderMenuDetails]] = [:]
        let Query = "Select menu_details.menu_id ,menu_name , menu_tarian_type , quantity , menu_price ,menu_isavailable, ordered_food_details.order_id FROM ordered_food_details INNER JOIN menu_details ON ordered_food_details.menu_id = menu_details.menu_id INNER JOIN orders ON orders.order_id = ordered_food_details.order_id INNER JOIN track_order ON track_order.order_id = orders.order_id AND orders.user_id = ? AND track_order.order_status != 'Delivered'  and track_order.order_status != 'Cancelled' and track_order.order_status != 'Declined' ;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(userId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderedMenuDetails = OrderMenuDetails()
            orderedMenuDetails.menuId = Int(sqlite3_column_int(QueryStatement, 0))
            orderedMenuDetails.menuName =   String(cString: sqlite3_column_text(QueryStatement, 1))
            orderedMenuDetails.menuTarianType =  String(cString: sqlite3_column_text(QueryStatement, 2))
            orderedMenuDetails.quantity =   Int(sqlite3_column_int(QueryStatement, 3))
            orderedMenuDetails.price =   Int(sqlite3_column_int (QueryStatement, 4))
            orderedMenuDetails.menuIsAvailable =   Int(sqlite3_column_int(QueryStatement, 5))
            orderedMenuDetails.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 6))
            menuDetailsInOrderedFoods[orderedMenuDetails.orderId] =  menuDetailsInOrderedFoods[orderedMenuDetails.orderId] ?? []
            menuDetailsInOrderedFoods[orderedMenuDetails.orderId]?.append(orderedMenuDetails)
            
        }
       
        
        sqlite3_finalize(QueryStatement)
        ()
        return menuDetailsInOrderedFoods
    }
    
//    func getMenuDetailsInpastOrderFoodDetails(orderIdListString : String) throws -> [ String: [OrderMenuDetails]]{
//        ()
//        var menuDetailsInOrderedFoods: [String :[OrderMenuDetails]] = [:]
//        let Query = "Select menu_details.menu_id ,menu_name , menu_tarian_type , quantity , menu_price ,menu_isavailable, order_id FROM ordered_food_details INNER JOIN menu_details ON ordered_food_details.menu_id = menu_details.menu_id where order_id IN \(orderIdListString);"
//        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
//
//        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
//            var orderedMenuDetails = OrderMenuDetails()
//            orderedMenuDetails.menuId = Int(sqlite3_column_int(QueryStatement, 0))
//            orderedMenuDetails.menuName =   String(cString: sqlite3_column_text(QueryStatement, 1))
//            orderedMenuDetails.menuTarianType =  String(cString: sqlite3_column_text(QueryStatement, 2))
//            orderedMenuDetails.quantity =   Int(sqlite3_column_int(QueryStatement, 3))
//            orderedMenuDetails.price =   Int(sqlite3_column_int (QueryStatement, 4))
//            orderedMenuDetails.menuIsAvailable =   Int(sqlite3_column_int(QueryStatement, 5))
//            orderedMenuDetails.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 6))
//            menuDetailsInOrderedFoods[orderedMenuDetails.orderId] =  menuDetailsInOrderedFoods[orderedMenuDetails.orderId] ?? []
//            menuDetailsInOrderedFoods[orderedMenuDetails.orderId]?.append(orderedMenuDetails)
//
//        }
//        sqlite3_finalize(QueryStatement)
//        ()
//        return menuDetailsInOrderedFoods
//    }
    
//    func updateOrderCancellationReason(orderId : String , orderCancellationReason : String) throws -> Bool{
//        ()
//        var updated : Bool = false
//        let query = "Update track_order SET order_cancellation_reason = ? where order_id = ? "
//        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
//        guard sqlite3_bind_text(queryStatement , 1, ( orderCancellationReason as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(queryStatement , 2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
//        else{
//                throw SQLiteError.Bind(message: errorMessage)
//            }
//        if( sqlite3_step(queryStatement) == SQLITE_DONE){
//            updated = true
//        } else{
//                throw SQLiteError.Step(message: errorMessage)
//            }
//        sqlite3_finalize(queryStatement)
//        ()
//        return updated
//    }
    
//    func getOrderCancellationReason(orderId : String) throws -> String{
//        ()
//        var orderCancellationReason = ""
//        let query = "SELECT order_cancellation_reason FROM track_order where order_id = ?;"
//        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
//        guard sqlite3_bind_text(queryStatement , 1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
//        else{
//                throw SQLiteError.Bind(message: errorMessage)
//            }
//        if sqlite3_step(queryStatement) != SQLITE_DONE{
//            if let reason = sqlite3_column_text(queryStatement, 0){
//                orderCancellationReason = String(cString: reason)
//            }
//        }
//
//        sqlite3_finalize(queryStatement)
//        ()
//        return orderCancellationReason
//    }
    
    func persistBillDetails(billDetails : BillDetailsModel) throws -> Bool{
        ()
        var isExecuted = false
        let insertQuery = "INSERT INTO bill_details(item_total, delivery_fee ,restaurant_gst, restaurant_packaging_charges , discount_amount,total_cost,order_id) VALUES (?,?,?,?,?,?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(billDetails.itemTotal)) == SQLITE_OK &&
                sqlite3_bind_int(insertQueryStatement , 2, Int32(billDetails.deliveryFee)) == SQLITE_OK &&
                sqlite3_bind_int(insertQueryStatement , 3, Int32(billDetails.restaurantGST)) == SQLITE_OK &&
                sqlite3_bind_double(insertQueryStatement , 4, Double(billDetails.restaurantPackagingCharges)) == SQLITE_OK &&
                sqlite3_bind_int(insertQueryStatement , 5, Int32(billDetails.itemDiscount)) == SQLITE_OK &&
                sqlite3_bind_int(insertQueryStatement , 6, Int32(billDetails.totalCost)) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 7, ( billDetails.orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
    }
    
//    func getBillDetailsOfOrder(orderId : String) throws -> BillDetailsModel{
//        ()
//        var billDetails : BillDetailsModel = BillDetailsModel()
//        let Query = "Select item_total, delivery_fee ,restaurant_gst, restaurant_packaging_charges , discount_amount,total_cost FROM bill_details WHERE order_id = ?;"
//        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
//        guard sqlite3_bind_text(QueryStatement , 1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
//        else{
//                throw SQLiteError.Bind(message: errorMessage)
//            }
//        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
//            billDetails.itemTotal = 
//            Int(sqlite3_column_int(QueryStatement, 0))
//            billDetails.deliveryFee =  Int(sqlite3_column_int(QueryStatement, 1))
//            billDetails.restaurantGST =  Double(sqlite3_column_double(QueryStatement, 2))
//            billDetails.restaurantPackagingCharges =  Int(sqlite3_column_int(QueryStatement, 3))
//            billDetails.itemDiscount =  Int(sqlite3_column_int(QueryStatement, 4))
//            billDetails.totalCost =  Int(sqlite3_column_int(QueryStatement, 5))
//            billDetails.orderId =  orderId
//            
//        }
//       
//        sqlite3_finalize(QueryStatement)
//        ()
//        return billDetails
//    }
    
//    func updateOrderStatus(orderId : String, orderStatus : OrderStatus) throws -> Bool{
//        ()
//        var statusUpdated = false
//        let query = "UPDATE track_order SET  order_status = ? where order_id = ? ;"
//        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
//
//        guard sqlite3_bind_text(queryStatement , 1, ( orderStatus.rawValue as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(queryStatement ,2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
//        else {
//            throw SQLiteError.Bind(message: errorMessage)
//        }
//
//
//        if(sqlite3_step(queryStatement) == SQLITE_DONE){
//            statusUpdated = true
//        }
//
//
//        sqlite3_finalize(queryStatement)
//        ()
//        return statusUpdated
//    }
//
    func getOrderStatus(orderId : String) throws -> OrderStatus{
        ()
        var orderStatus  : OrderStatus!
        let Query = "Select order_status FROM track_order where order_id = ?"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        guard  sqlite3_bind_text(QueryStatement ,1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }

        if sqlite3_step(QueryStatement) != SQLITE_DONE{
            orderStatus =  OrderStatus(rawValue: String(cString: sqlite3_column_text(QueryStatement, 0)))
        }
        else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(QueryStatement)
        ()
        return orderStatus
    }
    
//    func getRestaurantDetails(restaurantId: Int) throws -> RestaurantCompleteDetails {
//        ()
//        var restaurantContentDetails : RestaurantCompleteDetails  = RestaurantCompleteDetails()
//        let query = "SELECT restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description ,restaurant_phone_number , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_landmark ,restaurant_locality , restaurant_city , restaurant_state , restaurant_country,restaurant_pincode,restaurant_rating_out_of_5,restaurant_rating_out_of_5 ,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status,restaurant_food_packaging_charges FROM restaurant_accounts where restaurant_id = ?;"
//        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
//        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantId)) == SQLITE_OK
//            else {
//            throw SQLiteError.Bind(message: errorMessage)
//            }
//
//        while sqlite3_step(queryStatement) == SQLITE_ROW{
//            var restaurant = Restaurant()
//            var restaurantAddress = Address()
//            restaurantContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 0))
//            restaurant.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 1), length: Int(sqlite3_column_bytes(queryStatement, 1))) as Data
//            restaurant.restaurantName = String(cString: sqlite3_column_text(queryStatement, 2))
//            restaurant.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 3))
//            restaurant.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,4 ))
//            restaurant.restaurantContactNumber =  String(cString: sqlite3_column_text(queryStatement,5 ))
//            restaurantContentDetails.restaurant = restaurant
//            restaurantAddress.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement,6))
//            restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,7))
//            restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,8))
//            restaurantAddress.localityName =  String(cString: sqlite3_column_text(queryStatement,9))
//
//            restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,10))
//            restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,11))
//            restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,12))
//            restaurantAddress.pincode =  String(cString: sqlite3_column_text(queryStatement,13))
//            restaurantContentDetails.restaurantAddress =  restaurantAddress
//            restaurantContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,14 ))
//            restaurantContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,15))
//            restaurantContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,16))
//            restaurantContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,17))
//            restaurantContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,18))
//            restaurantContentDetails.restaurantFoodPackagingCharges =  Int( sqlite3_column_int(queryStatement,19))
//        }
//
//            sqlite3_finalize(queryStatement)
//        ()
//        return restaurantContentDetails
//    }
    
    func updateFoodRating(orderId : String , starRating : Int) throws -> Bool{
        ()
        var isExecuted = false
        let insertQuery = "UPDATE  food_rating SET star_rating = ? WHERE order_id = ?"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(starRating)) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
    }
    
    func updateRestaurantStarRating(starRating :Int , restaurantId : Int) throws -> Bool{
        ()
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_rating_out_of_5 =  ((restaurant_rating_out_of_5 * restaurant_total_number_of_ratings)  + ? ) / (restaurant_total_number_of_ratings + 1), restaurant_total_number_of_ratings = (restaurant_total_number_of_ratings + 1) WHERE restaurant_id = ?"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_int(queryStatement , 1, Int32(starRating)) == SQLITE_OK && sqlite3_bind_int(queryStatement , 2, Int32(restaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(queryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(queryStatement)
        ()
        return isExecuted
    }
    
    func updateFoodFeedback(orderId : String , feedback : String) throws -> Bool{
        ()
        var isExecuted = false
        let insertQuery = "UPDATE  food_rating SET feedback = ? WHERE order_id = ?"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement , 1, ( feedback as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement , 2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
    }
    
    func createFoodRatingRow(orderId : String) throws -> Bool{
        ()
        var isExecuted = false
        let insertQuery = "INSERT INTO food_rating(order_id) VALUES (?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement , 1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        ()
        return isExecuted
    }
    
    
    
   
}
