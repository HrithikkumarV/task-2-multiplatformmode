//
//  UserCartDAO.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 07/03/22.
//

import Foundation
import SQLite3

extension DatabaseService : UserCartDAOContract{
    
    
    func getRestaurantNameFromUserCart() throws -> String {
        
        var restaurantName = ""
        let Query = "Select restaurant_name FROM user_cart INNER JOIN restaurant_accounts ON user_cart.restaurant_id = restaurant_accounts.restaurant_id;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        if sqlite3_step(QueryStatement) != SQLITE_DONE{
            restaurantName =  String(cString: sqlite3_column_text(QueryStatement, 0))
        }
        else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(QueryStatement)
        
        return restaurantName
    }
    

    func getCartMenuItems() throws -> [Int : Int] {
        
        var menusInCart : [Int : Int] = [:]
            let query = "SELECT menu_id , quantity FROM user_cart;"
            let queryStatement : OpaquePointer? = try prepareStatement(query: query)
                while sqlite3_step(queryStatement) == SQLITE_ROW{
                    menusInCart[Int(sqlite3_column_int(queryStatement, 0))] = Int(sqlite3_column_int(queryStatement, 1))
                }
            
            sqlite3_finalize(queryStatement)
            
            return menusInCart
    }
    
    func insertMenuToCart(cartDetails: (menuId: Int, restaurantId: Int, quantity: Int)) throws -> Bool {
        
        var isExecuted = false
        let insertQuery = "INSERT INTO user_cart(menu_id,quantity,restaurant_id) VALUES (?,?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(cartDetails.menuId)) == SQLITE_OK && sqlite3_bind_int(insertQueryStatement , 2, Int32(cartDetails.quantity)) == SQLITE_OK &&
                sqlite3_bind_int(insertQueryStatement , 3, Int32(cartDetails.restaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
        
    }
    
    func updateMenuInCart(menuId: Int, quantity: Int) throws -> Bool{
        
        var cartUpdated = false
        let query = "UPDATE user_cart SET  quantity = ? where menu_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement, 1, Int32(quantity) ) == SQLITE_OK && sqlite3_bind_int(queryStatement, 2, Int32(menuId) ) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }

        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            cartUpdated = true
        }
        
        
        sqlite3_finalize(queryStatement)
        
        return cartUpdated
    }
    
    
    
    
    func removeMenuFromCart(menuId: Int) throws -> Bool{
        
        var isExecuted = false
        let Query = "DELETE FROM user_cart WHERE menu_id = ?;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement, 1, Int32(menuId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(QueryStatement) == SQLITE_DONE{
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func clearCart() throws -> Bool {
        
        var isExecuted = false
        let Query = "DELETE FROM user_cart;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted  = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getMenuPrice(menuId: Int) throws -> Int{
        
        var menuPrice = 0
            let query = "SELECT menu_price FROM menu_details where menu_id = ?;"
            let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            guard sqlite3_bind_int(queryStatement, 1, Int32(menuId)) == SQLITE_OK
                else {
                throw SQLiteError.Bind(message: errorMessage)
                }
                if sqlite3_step(queryStatement) == SQLITE_ROW{
                    menuPrice = Int(sqlite3_column_int(queryStatement, 0))
                }
            
            sqlite3_finalize(queryStatement)
            ()
            return menuPrice
    }
    
    func getRestaurantDetailsFromUserCart() throws -> RestaurantCompleteDetails {
        
        var restaurantsContentDetails : RestaurantCompleteDetails = RestaurantCompleteDetails()
        let query = "SELECT restaurant_accounts.restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_locality, restaurant_landmark, restaurant_city , restaurant_state , restaurant_country ,restaurant_pincode,restaurant_rating_out_of_5, restaurant_total_number_of_ratings,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status,restaurant_food_packaging_charges FROM user_cart INNER JOIN restaurant_accounts ON user_cart.restaurant_id = restaurant_accounts.restaurant_id; ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var restaurant = Restaurant()
            var restaurantAddress = Address()
            restaurantsContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 0))
            restaurant.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 1), length: Int(sqlite3_column_bytes(queryStatement, 1))) as Data
            restaurant.restaurantName = String(cString: sqlite3_column_text(queryStatement, 2))
            restaurant.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 3))
            restaurant.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,4 ))
            restaurantsContentDetails.restaurant = restaurant
            restaurantAddress.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement,5))
            restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,6))
            restaurantAddress.localityName =  String(cString: sqlite3_column_text(queryStatement,7))
            restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,8))
            restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,9))
            restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,10))
            restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,11))
            restaurantAddress.pincode =  String(cString: sqlite3_column_text(queryStatement,12))
            restaurantsContentDetails.restaurantAddress =  restaurantAddress
            restaurantsContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,13 ))
            restaurantsContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,14))
            restaurantsContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,15))
            restaurantsContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,16))
            restaurantsContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,17))
            restaurantsContentDetails.restaurantFoodPackagingCharges =  Int( sqlite3_column_int(queryStatement,18))
        }
        
            sqlite3_finalize(queryStatement)
        
        return restaurantsContentDetails
    }
    
    func getMenuDetailsInCart() throws -> [CartMenuDetails]{
        
        var menuDetailsInCart : [CartMenuDetails] = []
        let Query = "Select menu_details.menu_id ,menu_name , menu_tarian_type , quantity , menu_price ,menu_isavailable , menu_next_available_at FROM user_cart INNER JOIN menu_details ON user_cart.menu_id = menu_details.menu_id;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var menuDetails : CartMenuDetails = CartMenuDetails()
                
            menuDetails.menuId =  Int(sqlite3_column_int(QueryStatement, 0))
            menuDetails.menuName =  String(cString: sqlite3_column_text(QueryStatement, 1))
            menuDetails.menuTarianType =  String(cString: sqlite3_column_text(QueryStatement, 2))
            menuDetails.quantity =  Int(sqlite3_column_int(QueryStatement, 3))
            menuDetails.price =   Int(sqlite3_column_int (QueryStatement, 4))
            menuDetails.menuIsAvailable =  Int(sqlite3_column_int(QueryStatement, 5))
            menuDetails.menuNextAvailableAt =   String(cString: sqlite3_column_text(QueryStatement, 6))
            menuDetailsInCart.append(menuDetails)
            
        }
       
        sqlite3_finalize(QueryStatement)
        
        return menuDetailsInCart
    }
    
    
    
}
