//
//  SearchMenuAndRestaurantDAO.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 28/02/22.
//

import Foundation
import SQLite3

extension DatabaseService : SearchMenuAndRestaurantDAOContract {
    
    
    func getMenusForSearchedItem(searchItem: String, locality: String , pincode : String) throws -> [(menu : MenuContentDetails,restaurant : RestaurantCompleteDetails)] {
        
        var menuDetailsWithRestaurantRestaurantDetailsList : [(menu : MenuContentDetails,restaurant : RestaurantCompleteDetails)] = []
            
            let query = "SELECT menu_name , menu_description , menu_price , category_id , menu_tarian_type , menu_image , menu_id, menu_isavailable,menu_next_available_at, menu_status,restaurant_accounts.restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_landmark, restaurant_city , restaurant_state , restaurant_country ,restaurant_rating_out_of_5,restaurant_total_number_of_ratings ,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status FROM  menu_details INNER JOIN restaurant_accounts ON menu_details.restaurant_id = restaurant_accounts.restaurant_id and ( restaurant_locality = ?  and restaurant_pincode = ? and menu_name LIKE ?  and restaurant_account_status = 1  and menu_status = 1);"
            let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            
        guard sqlite3_bind_text(queryStatement , 1, (locality as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (pincode as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, ("%" + searchItem + "%" as NSString).utf8String, -1, nil) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                
                var menuDetailsWithRestaurantDetails : (menu : MenuContentDetails,restaurant : RestaurantCompleteDetails)
                var menuContentDetails = MenuContentDetails()
                var menuDetails = MenuDetails()
                
                menuDetails.menuName = String(cString: sqlite3_column_text(queryStatement, 0))
                menuDetails.menuDescription = String(cString: sqlite3_column_text(queryStatement, 1))
                menuDetails.menuPrice =  Int(sqlite3_column_int(queryStatement, 2))
                menuDetails.menuCategoryId =  Int(sqlite3_column_int(queryStatement, 3))
                menuDetails.menuTarianType =  String(cString: sqlite3_column_text(queryStatement, 4))
                let length : Int? = Int(sqlite3_column_bytes(queryStatement, 5))
                let menuImage  : NSData? = NSData(bytes: sqlite3_column_blob(queryStatement, 5), length: length ?? 0)
                if  length! != 0 {
                    menuDetails.menuImage =  menuImage! as Data
                }
                menuContentDetails.menuDetails =  menuDetails
                menuContentDetails.menuId =  Int(sqlite3_column_int(queryStatement, 6))
                menuContentDetails.menuIsAvailable =  Int(sqlite3_column_int(queryStatement, 7))
                menuContentDetails.menuNextAvailableAt =  String(cString: sqlite3_column_text(queryStatement, 8))
                menuContentDetails.menuStatus =  Int(sqlite3_column_int(queryStatement, 9))
                
                menuDetailsWithRestaurantDetails.menu = menuContentDetails
                
                var restaurantContentDetails = RestaurantCompleteDetails()
                var restaurant = Restaurant()
                var restaurantAddress = Address()
                restaurantContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 10))
                restaurant.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 11), length: Int(sqlite3_column_bytes(queryStatement, 11))) as Data
                restaurant.restaurantName = String(cString: sqlite3_column_text(queryStatement, 12))
                restaurant.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 13))
                restaurant.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,14 ))
                restaurantContentDetails.restaurant = restaurant
                restaurantAddress.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement,15))
                restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,16))
                restaurantAddress.localityName =  locality
                restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,17))
                restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,18))
                restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,19))
                restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,20))
                restaurantAddress.pincode =  pincode
                restaurantContentDetails.restaurantAddress =  restaurantAddress
                restaurantContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,21 ))
                restaurantContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,22))
                restaurantContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,23))
                restaurantContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,24))
                restaurantContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,25))
                restaurantContentDetails.isDeliverable =  true
                menuDetailsWithRestaurantDetails.restaurant = restaurantContentDetails
                menuDetailsWithRestaurantRestaurantDetailsList.append(menuDetailsWithRestaurantDetails)
            }
            
                sqlite3_finalize(queryStatement)
            
            return menuDetailsWithRestaurantRestaurantDetailsList
    }
    
    func getRestaurantsForSearchedItem(searchItem: String, locality: String , pincode : String) throws -> [RestaurantCompleteDetails] {
       
        var restaurantsContentDetails : [RestaurantCompleteDetails] = []
        let query = "SELECT restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_landmark, restaurant_city , restaurant_state , restaurant_country ,restaurant_rating_out_of_5,restaurant_total_number_of_ratings ,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status FROM restaurant_accounts where restaurant_locality = ? and restaurant_pincode = ?  and  restaurant_name LIKE ? and restaurant_account_status = 1;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (locality as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (pincode as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, ("%" + searchItem + "%" as NSString).utf8String, -1, nil) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var restaurantContentDetails = RestaurantCompleteDetails()
            var restaurant = Restaurant()
            var restaurantAddress = Address()
            restaurantContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 0))
            restaurant.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 1), length: Int(sqlite3_column_bytes(queryStatement, 1))) as Data
            restaurant.restaurantName = String(cString: sqlite3_column_text(queryStatement, 2))
            restaurant.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 3))
            restaurant.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,4 ))
            restaurantContentDetails.restaurant = restaurant
            restaurantAddress.doorNoAndBuildingNameAndBuildingNo  = String(cString: sqlite3_column_text(queryStatement,5))
            restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,6))
            restaurantAddress.localityName =  locality
            restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,7))
            restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,8))
            restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,9))
            restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,10))
            restaurantAddress.pincode =  pincode
            restaurantContentDetails.restaurantAddress =  restaurantAddress
            restaurantContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,11 ))
            restaurantContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,12))
            restaurantContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,13))
            restaurantContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,14))
            restaurantContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,15))
            restaurantContentDetails.isDeliverable =  true
            restaurantsContentDetails.append(restaurantContentDetails)
        }
        
            sqlite3_finalize(queryStatement)
        
        return restaurantsContentDetails
    }
    
    func getMenusForSearchedItemSuggestion(searchItem: String, locality: String , pincode : String) throws -> Set<String>{
        
        var menuNameList : Set<String> = []
            
            let query = "SELECT menu_name FROM  menu_details INNER JOIN restaurant_accounts ON menu_details.restaurant_id = restaurant_accounts.restaurant_id and ( restaurant_locality = ?  and restaurant_pincode = ? and menu_name LIKE ?  and restaurant_account_status = 1  and menu_status = 1);"
            let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            
        guard sqlite3_bind_text(queryStatement , 1, (locality as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (pincode as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, ("%" + searchItem + "%" as NSString).utf8String, -1, nil) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                
                
                menuNameList.insert(String(cString: sqlite3_column_text(queryStatement, 0)))
            }
            
                sqlite3_finalize(queryStatement)
            
            return menuNameList
    }
    
    func getRestaurantsForSearchedItemSuggestion(searchItem: String, locality: String , pincode : String) throws -> Set<String> {
       
        var restaurantsList : Set<String> = []
        let query = "SELECT  restaurant_name FROM restaurant_accounts where restaurant_locality = ? and restaurant_pincode = ?  and  restaurant_name LIKE ? and restaurant_account_status = 1;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (locality as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (pincode as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, ("%" + searchItem + "%" as NSString).utf8String, -1, nil) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            
            restaurantsList.insert(String(cString: sqlite3_column_text(queryStatement, 0)))
        }
        
            sqlite3_finalize(queryStatement)
        
        return restaurantsList
    }
    
    func persistSearchHistory(searchItemName : String , searchItemType : SearchItemType) throws -> Bool{
        
        
        var isExecuted = false
        let insertQuery = "INSERT INTO search_history(search_item_name,search_item_type) VALUES (?,?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement, 1, (searchItemName as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(insertQueryStatement, 2, (searchItemType.rawValue as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        }
            else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
       
        return isExecuted
    }
    
    func clearSearchHistory() throws -> Bool{
        
        var isExecuted = false
        let Query = "DELETE FROM search_history;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
       
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getSearchHistoryItems() throws -> [(searchItemName : String , searchItemType : SearchItemType)] {
        
        var seacrhItems : [(searchItemName : String , searchItemType : SearchItemType)] = []
        let query = "SELECT search_item_name , search_item_type FROM search_history ORDER BY search_history_id DESC;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                seacrhItems.append((searchItemName : String(cString:  sqlite3_column_text(queryStatement, 0)) , searchItemType : SearchItemType(rawValue: String(cString:  sqlite3_column_text(queryStatement, 1)))!))
            }
        sqlite3_finalize(queryStatement)
        
        return seacrhItems
    }
    
    func getSearchHistoryItemsLikeTheSearchItem(searchItem :String) throws -> [(searchItemName : String , searchItemType : SearchItemType)] {
        
        var seacrhItems : [(searchItemName : String , searchItemType : SearchItemType)] = []
        let query = "SELECT search_item_name , search_item_type FROM search_history WHERE search_item_name LIKE  ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, ("%" + searchItem + "%" as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
            throw SQLiteError.Bind(message: errorMessage)
        }
        
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                seacrhItems.append((searchItemName : String(cString:  sqlite3_column_text(queryStatement, 0)) , searchItemType : SearchItemType(rawValue: String(cString:  sqlite3_column_text(queryStatement, 1)))!))
            }
        sqlite3_finalize(queryStatement)
        
        return seacrhItems
    }
    
    func clearSearchHistoryItem(searchHistoryId : Int) throws -> Bool{
        
        var isExecuted = false
        let Query = "DELETE FROM search_history WHERE search_history_id = ?;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(searchHistoryId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
}


