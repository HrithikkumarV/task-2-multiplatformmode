//
//  CouponDAO.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 14/03/22.
//

import Foundation
import SQLite3

extension DatabaseService : CouponDAOContract{
    
    func persistCouponCode(couponCode: String) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO coupon_applied(coupon_code) VALUES (?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement, 1, (couponCode as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted = true
        }
            else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }
    
    func deleteCouponCode() throws -> Bool{
        
        var isExecuted = false
        let Query = "DELETE FROM coupon_applied;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
        sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getCouponCode() throws -> String {
        var couponCode = ""
        let query = "SELECT coupon_code FROM coupon_applied;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                couponCode =  String(cString:  sqlite3_column_text(queryStatement, 0))
            }
        
        sqlite3_finalize(queryStatement)
        
        return couponCode
    }
    
}
