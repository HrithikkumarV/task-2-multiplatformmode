//
//  RestaurantsDisplayDetailsDAO.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 19/01/22.
//

import Foundation
import SQLite3

extension DatabaseService : RestaurantsDAOContracts {
    
    
    func getRestaurentsTabContentDetails(locality: String , pincode : String) throws -> [RestaurantCompleteDetails] {
        
        var restaurantsTabDisplayDetails : [RestaurantCompleteDetails] = []
        let query = "SELECT restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description ,restaurant_phone_number , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_landmark, restaurant_city , restaurant_state , restaurant_country ,restaurant_rating_out_of_5,restaurant_total_number_of_ratings  ,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status,restaurant_food_packaging_charges FROM restaurant_accounts where restaurant_locality = ? and restaurant_pincode = ? and restaurant_account_status = 1 ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_text(queryStatement , 1, (locality as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (pincode as NSString).utf8String, -1, nil) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var restaurantTabContentDetails = RestaurantCompleteDetails()
            var restaurant = Restaurant()
            var restaurantAddress = Address()
            restaurantTabContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 0))
            restaurant.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 1), length: Int(sqlite3_column_bytes(queryStatement, 1))) as Data
            restaurant.restaurantName = String(cString: sqlite3_column_text(queryStatement, 2))
            restaurant.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 3))
            restaurant.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,4 ))
            restaurant.restaurantContactNumber =  String(cString: sqlite3_column_text(queryStatement,5 ))
            restaurantTabContentDetails.restaurant = restaurant
            restaurantAddress.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement,6))
            restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,7))
            restaurantAddress.localityName =  locality
            restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,8))
            restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,9))
            restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,10))
            restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,11))
            restaurantAddress.pincode =  pincode
            restaurantTabContentDetails.restaurantAddress =  restaurantAddress
            restaurantTabContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,12))
            restaurantTabContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,13))
            restaurantTabContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,14))
            restaurantTabContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,15))
            restaurantTabContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,16))
            restaurantTabContentDetails.restaurantFoodPackagingCharges =  Int( sqlite3_column_int(queryStatement,17))
            restaurantsTabDisplayDetails.append(restaurantTabContentDetails)
        }
        
            sqlite3_finalize(queryStatement)
      
        return restaurantsTabDisplayDetails
    }
    
    
}

