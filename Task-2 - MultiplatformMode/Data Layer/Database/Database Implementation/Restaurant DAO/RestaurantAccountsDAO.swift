//
//  RestaurantCreateAccount.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/12/21.
//

import Foundation
import SQLite3

extension DatabaseService : RestaurantAccountsDAOContracts{
    
    
    
    func checkIfRestaurantPhoneNumberExists(phoneNumber: String) throws -> Bool {
        
        var isExist = false
        let query = "SELECT restaurant_id FROM restaurant_accounts where restaurant_phone_number = (?);"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
            if(sqlite3_step(queryStatement) == SQLITE_ROW){
                isExist = true
            }
        
        
            sqlite3_finalize(queryStatement)
        
        return isExist
    }
    
    
    
    func persistRestaurantCreateAccount(restaurantAccount : RestaurantAccount,restaurant : Restaurant ,restaurantAddress : Address) throws -> Bool{
        
        let insertQuery = "INSERT INTO restaurant_accounts(restaurant_name , restaurant_phone_number, resaurant_password ,restaurant_cuisine, restaurant_description ,restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_locality, restaurant_landmark, restaurant_city , restaurant_state , restaurant_country,restaurant_pincode ,restaurant_image ,restaurant_joined_at) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, datetime('now','localtime'))"
        var isSuccessfullySignedUp : Bool = false
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_text(insertQueryStatement , 1, (restaurant.restaurantName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 2, (restaurantAccount.restaurantPhoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(insertQueryStatement , 3, (restaurantAccount.restaurantPassword as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 4, (restaurant.restaurantCuisine as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 5, (restaurant.restaurantDescription as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 6, (restaurantAddress.doorNoAndBuildingNameAndBuildingNo as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 7, (restaurantAddress.streetName as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 8, (restaurantAddress.localityName as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 9, (restaurantAddress.landmark as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 10, (restaurantAddress.city as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 11, (restaurantAddress.state as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 12, (restaurantAddress.country as NSString).utf8String, -1, nil) == SQLITE_OK &&
        sqlite3_bind_text(insertQueryStatement , 13, (restaurantAddress.pincode as NSString).utf8String, -1, nil) == SQLITE_OK
                && restaurant.restaurantProfileImage.withUnsafeBytes({ buffer -> Int32 in
                    sqlite3_bind_blob(insertQueryStatement, 14,buffer.baseAddress, Int32(restaurant.restaurantProfileImage.count), nil)
                }) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if(sqlite3_step(insertQueryStatement) == SQLITE_DONE){
            isSuccessfullySignedUp = true
        }
        else{
                throw SQLiteError.Step(message: errorMessage)
                
            }
        
            sqlite3_finalize(insertQueryStatement)
        
        return isSuccessfullySignedUp
    }
    
    
    
    func getRestaurantPassword(phoneNumber : String) throws -> String {
        
        var password : String = ""
        let query = "SELECT resaurant_password FROM restaurant_accounts where restaurant_phone_number = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
            if(sqlite3_step(queryStatement) == SQLITE_ROW){
                password = String(cString: sqlite3_column_text(queryStatement, 0))
            }
        
        
            sqlite3_finalize(queryStatement)
        
        return password
    }
    
    func getRestaurantId(phoneNumber : String) throws -> Int {
        
        var restaurantId : Int = 0
        let query = "SELECT  restaurant_id  FROM restaurant_accounts where restaurant_phone_number = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
            if(sqlite3_step(queryStatement) == SQLITE_ROW){
                restaurantId = Int( sqlite3_column_int(queryStatement, 0))
            }
        
        
            sqlite3_finalize(queryStatement)
        
        return restaurantId
    }
    
    
    func UpdatePassword(phoneNumber : String , password : String) throws -> Bool{
        
        var passwordUpdated = false
        let query = "UPDATE restaurant_accounts SET resaurant_password = ? where restaurant_phone_number = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (password as NSString).utf8String, -1, nil) == SQLITE_OK &&
            sqlite3_bind_text(queryStatement , 2, (phoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            passwordUpdated = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return passwordUpdated
    }
    
    
    
    func persistRestaurantId(restaurantId: Int) throws -> Bool{
        
        var isExecuted = false
        let insertQuery = "INSERT INTO local_persist_restaurant_Id(restaurant_id) VALUES (?)"
        let insertQueryStatement : OpaquePointer? = try prepareStatement(query: insertQuery)
        guard sqlite3_bind_int(insertQueryStatement , 1, Int32(restaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if sqlite3_step(insertQueryStatement) == SQLITE_DONE {
            isExecuted  = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(insertQueryStatement)
        
        return isExecuted
    }
    
    func deleteRestaurantId() throws -> Bool {
        
        var isExecuted = false
        let Query = "DELETE FROM local_persist_restaurant_Id;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        
        if sqlite3_step(QueryStatement) == SQLITE_DONE {
            isExecuted = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        
            sqlite3_finalize(QueryStatement)
        
        return isExecuted
    }
    
    func getRestaurantId() throws -> Int {
        
        var restaurantId = 0
        let query = "SELECT restaurant_id FROM local_persist_restaurant_Id;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                restaurantId =  Int(sqlite3_column_int(queryStatement, 0))
            }
            
        sqlite3_finalize(queryStatement)
        
        return restaurantId
    }
    
   
    func getRestaurantDetails(restaurantId: Int) throws -> RestaurantCompleteDetails {
        
        var restaurantContentDetails : RestaurantCompleteDetails  = RestaurantCompleteDetails()
        let query = "SELECT restaurant_id ,restaurant_image , restaurant_name, restaurant_cuisine, restaurant_description ,restaurant_phone_number , restaurant_door_number_and_building_name_and_building_number, restaurant_street_name , restaurant_landmark ,restaurant_locality , restaurant_city , restaurant_state , restaurant_country,restaurant_pincode,restaurant_rating_out_of_5,restaurant_total_number_of_ratings ,restaurant_isavailable, restaurant_opens_next_at, restaurant_account_status,restaurant_food_packaging_charges FROM restaurant_accounts where restaurant_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantId)) == SQLITE_OK
            else {
            throw SQLiteError.Bind(message: errorMessage)
            }
        
        while sqlite3_step(queryStatement) == SQLITE_ROW{
            var restaurant = Restaurant()
            var restaurantAddress = Address()
            restaurantContentDetails.restaurantId =   Int(sqlite3_column_int(queryStatement, 0))
            restaurant.restaurantProfileImage =  NSData(bytes: sqlite3_column_blob(queryStatement, 1), length: Int(sqlite3_column_bytes(queryStatement, 1))) as Data
            restaurant.restaurantName = String(cString: sqlite3_column_text(queryStatement, 2))
            restaurant.restaurantCuisine =  String(cString: sqlite3_column_text(queryStatement, 3))
            restaurant.restaurantDescription =  String(cString: sqlite3_column_text(queryStatement,4 ))
            restaurant.restaurantContactNumber =  String(cString: sqlite3_column_text(queryStatement,5 ))
            restaurantContentDetails.restaurant = restaurant
            restaurantAddress.doorNoAndBuildingNameAndBuildingNo = String(cString: sqlite3_column_text(queryStatement,6))
            restaurantAddress.streetName =  String(cString: sqlite3_column_text(queryStatement,7))
            restaurantAddress.landmark =  String(cString: sqlite3_column_text(queryStatement,8))
            restaurantAddress.localityName =  String(cString: sqlite3_column_text(queryStatement,9))
            
            restaurantAddress.city =  String(cString: sqlite3_column_text(queryStatement,10))
            restaurantAddress.state =  String(cString: sqlite3_column_text(queryStatement,11))
            restaurantAddress.country =  String(cString: sqlite3_column_text(queryStatement,12))
            restaurantAddress.pincode =  String(cString: sqlite3_column_text(queryStatement,13))
            restaurantContentDetails.restaurantAddress =  restaurantAddress
            restaurantContentDetails.restaurantStarRating =  String(cString: sqlite3_column_text(queryStatement,14 ))
            restaurantContentDetails.totalNumberOfRating =  Int( sqlite3_column_int(queryStatement,15))
            restaurantContentDetails.restaurantIsAvailable =  Int( sqlite3_column_int(queryStatement,16))
            restaurantContentDetails.restaurantOpensNextAt =  String(cString: sqlite3_column_text(queryStatement,17))
            restaurantContentDetails.restaurantAccountStatus =  Int( sqlite3_column_int(queryStatement,18))
            
            restaurantContentDetails.restaurantFoodPackagingCharges =  Int( sqlite3_column_int(queryStatement,19))
        }
        
            sqlite3_finalize(queryStatement)
        
        return restaurantContentDetails
    }
    
    func updateRestaurantName(restaurantID: Int, restaurantName: String) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_name = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (restaurantName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantProfileImage(restaurantID: Int, profileImage: Data) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_image = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard profileImage.withUnsafeBytes({ buffer -> Int32 in
            sqlite3_bind_blob(queryStatement, 1,buffer.baseAddress, Int32(profileImage.count), nil)
        }) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantCuisine(restaurantID: Int, restaurantCuisine: String) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_cuisine = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (restaurantCuisine as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantDescription(restaurantID: Int, restaurantDescription: String) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_description = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (restaurantDescription as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantPhoneNumber(restaurantID: Int, restaurantPhoneNumber: String) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_phone_number = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, (restaurantPhoneNumber as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantAvailablityAndNextOpensAt(restaurantID: Int, restaurantIsAvailable : Int,restaurantOpensNextAt: String) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_isavailable = ? , restaurant_opens_next_at = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantIsAvailable)) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (restaurantOpensNextAt as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 3, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    
    
    func updateRestaurantStatus(restaurantID: Int, restaurantStatus : Int) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET  restaurant_account_status = ? where restaurant_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantStatus)) == SQLITE_OK &&
            sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantFoodPackagingCharges(restaurantID: Int, restaurantFoodPackagingCharge : Int) throws -> Bool {
        
        var isExecuted = false
        let query = "UPDATE restaurant_accounts SET restaurant_food_packaging_charges = ? where restaurant_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_int(queryStatement , 1, Int32(restaurantFoodPackagingCharge)) == SQLITE_OK &&
                sqlite3_bind_int(queryStatement , 2, Int32(restaurantID)) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            isExecuted = true
        }
        
        sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    func updateRestaurantAddress(restaurantID: Int, restaurantAddress: Address) throws -> Bool {
        
        var isExecuted  : Bool = false
        let query = "UPDATE restaurant_accounts SET  restaurant_door_number_and_building_name_and_building_number = ?, restaurant_street_name = ?, restaurant_landmark = ? ,restaurant_locality = ? , restaurant_city = ? , restaurant_state = ? , restaurant_country = ?,restaurant_pincode = ? where restaurant_id = ?"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard
                
                sqlite3_bind_text(queryStatement , 1, (restaurantAddress.doorNoAndBuildingNameAndBuildingNo as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 2, (restaurantAddress.streetName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 3, (restaurantAddress.localityName as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 4, (restaurantAddress.landmark as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 5, (restaurantAddress.city as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 6, (restaurantAddress.state as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 7, (restaurantAddress.country as NSString).utf8String, -1, nil) == SQLITE_OK &&
                sqlite3_bind_text(queryStatement , 8, (restaurantAddress.pincode as NSString).utf8String, -1, nil) == SQLITE_OK
                    &&
                           sqlite3_bind_int(queryStatement , 9, Int32(restaurantID)) == SQLITE_OK
                    
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(queryStatement) == SQLITE_DONE{
            isExecuted = true
        }
        else{
            throw SQLiteError.Step(message: errorMessage)
        }
            sqlite3_finalize(queryStatement)
        
        return isExecuted
    }
    
    
}
