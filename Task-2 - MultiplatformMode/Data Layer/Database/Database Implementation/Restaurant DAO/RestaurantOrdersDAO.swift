//
//  RestaurantOrderDetails.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 15/03/22.
//

import Foundation
import SQLite3

extension DatabaseService : RestaurantOrdersDAOContract{
    
   
    func updateOrderStatus(orderId : String, orderStatus : OrderStatus) throws -> Bool{
        
        var statusUpdated = false
        let query = "UPDATE track_order SET  order_status = ? where order_id = ? ;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        
        guard sqlite3_bind_text(queryStatement , 1, ( orderStatus.rawValue as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(queryStatement ,2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }

        
        if(sqlite3_step(queryStatement) == SQLITE_DONE){
            statusUpdated = true
        }
        
        
        sqlite3_finalize(queryStatement)
        
        return statusUpdated
    }
    
    
    func getMenuDetailsInOrderFoodDetails(orderId: String) throws -> [OrderMenuDetails] {
        
        var menuDetailsInOrderedFoods: [OrderMenuDetails] = []
        let Query = "Select menu_details.menu_id ,menu_name , menu_tarian_type , quantity , menu_price ,menu_isavailable FROM ordered_food_details INNER JOIN menu_details ON ordered_food_details.menu_id = menu_details.menu_id AND order_id = ?;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_text(QueryStatement , 1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderedMenuDetails = OrderMenuDetails()
            orderedMenuDetails.menuId = Int(sqlite3_column_int(QueryStatement, 0))
            orderedMenuDetails.menuName =   String(cString: sqlite3_column_text(QueryStatement, 1))
            orderedMenuDetails.menuTarianType =  String(cString: sqlite3_column_text(QueryStatement, 2))
            orderedMenuDetails.quantity =   Int(sqlite3_column_int(QueryStatement, 3))
            orderedMenuDetails.price =   Int(sqlite3_column_int (QueryStatement, 4))
            orderedMenuDetails.menuIsAvailable =   Int(sqlite3_column_int(QueryStatement, 5))
            orderedMenuDetails.orderId =  orderId
            menuDetailsInOrderedFoods.append(orderedMenuDetails)
            
        }
       
        sqlite3_finalize(QueryStatement)
        
        return menuDetailsInOrderedFoods
    }
    
   
    func getMenuDetailsInActiveOrderFoodDetails(restaurantId : Int) throws -> [ String : [OrderMenuDetails]]{
        
        var menuDetailsInOrderedFoods: [ String :[OrderMenuDetails]] = [:]
        let Query = "Select menu_details.menu_id ,menu_name , menu_tarian_type , quantity , menu_price ,menu_isavailable, ordered_food_details.order_id FROM ordered_food_details INNER JOIN menu_details ON ordered_food_details.menu_id = menu_details.menu_id INNER JOIN orders ON orders.order_id = ordered_food_details.order_id INNER JOIN track_order ON track_order.order_id = orders.order_id AND orders.restaurant_id = ? AND track_order.order_status != 'Delivered'  and track_order.order_status != 'Cancelled' and track_order.order_status != 'Declined';"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(restaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderedMenuDetails = OrderMenuDetails()
            orderedMenuDetails.menuId = Int(sqlite3_column_int(QueryStatement, 0))
            orderedMenuDetails.menuName =   String(cString: sqlite3_column_text(QueryStatement, 1))
            orderedMenuDetails.menuTarianType =  String(cString: sqlite3_column_text(QueryStatement, 2))
            orderedMenuDetails.quantity =   Int(sqlite3_column_int(QueryStatement, 3))
            orderedMenuDetails.price =   Int(sqlite3_column_int (QueryStatement, 4))
            orderedMenuDetails.menuIsAvailable =   Int(sqlite3_column_int(QueryStatement, 5))
            orderedMenuDetails.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 6))
            menuDetailsInOrderedFoods[orderedMenuDetails.orderId] =  menuDetailsInOrderedFoods[orderedMenuDetails.orderId] ?? []
            menuDetailsInOrderedFoods[orderedMenuDetails.orderId]?.append(orderedMenuDetails)
            
        }
       
        sqlite3_finalize(QueryStatement)
        
        return menuDetailsInOrderedFoods
    }
    
    func getMenuDetailsInpastOrderFoodDetails(orderIdListString : String) throws -> [ String: [OrderMenuDetails]]{
        
        var menuDetailsInOrderedFoods: [String :[OrderMenuDetails]] = [:]
        let Query = "Select menu_details.menu_id ,menu_name , menu_tarian_type , quantity , menu_price ,menu_isavailable, order_id FROM ordered_food_details INNER JOIN menu_details ON ordered_food_details.menu_id = menu_details.menu_id where order_id IN \(orderIdListString);"
        print(Query)
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
       
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderedMenuDetails = OrderMenuDetails()
            orderedMenuDetails.menuId = Int(sqlite3_column_int(QueryStatement, 0))
            orderedMenuDetails.menuName =   String(cString: sqlite3_column_text(QueryStatement, 1))
            orderedMenuDetails.menuTarianType =  String(cString: sqlite3_column_text(QueryStatement, 2))
            orderedMenuDetails.quantity =   Int(sqlite3_column_int(QueryStatement, 3))
            orderedMenuDetails.price =   Int(sqlite3_column_int (QueryStatement, 4))
            orderedMenuDetails.menuIsAvailable =   Int(sqlite3_column_int(QueryStatement, 5))
            orderedMenuDetails.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 6))
            menuDetailsInOrderedFoods[orderedMenuDetails.orderId] =  menuDetailsInOrderedFoods[orderedMenuDetails.orderId] ?? []
            menuDetailsInOrderedFoods[orderedMenuDetails.orderId]?.append(orderedMenuDetails)
            
        }
        sqlite3_finalize(QueryStatement)
        
        return menuDetailsInOrderedFoods
    }
    
    
    
    func getActiveOrderDetails(restaurantId : Int)throws -> [OrderDetails]{
        
        var ordersDetails : [OrderDetails] = []
        let Query = "Select orders.order_id, user_id , restaurant_id ,user_address_id ,date_and_time,order_status , payment_amount,user_instruction_to_restaurant FROM orders INNER JOIN track_order ON track_order.order_id = orders.order_id INNER JOIN payment_details ON payment_details.payment_id = orders.payment_id INNER JOIN  user_instruction_to_restaurant ON user_instruction_to_restaurant.order_id = orders.order_id WHERE  restaurant_id = ? and order_status != 'Delivered' and order_status != 'Cancelled' and order_status != 'Declined';"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(restaurantId)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderDetails = OrderDetails()
            orderDetails.orderModel.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 0))
            orderDetails.orderModel.userId =  Int(sqlite3_column_int(QueryStatement, 1))
            orderDetails.orderModel.restaurantId =  Int(sqlite3_column_int(QueryStatement, 2))
            orderDetails.orderModel.userAddressId =  String(cString: sqlite3_column_text(QueryStatement, 3))
            orderDetails.orderModel.dateAndTime =  String(cString: sqlite3_column_text(QueryStatement, 4))
            orderDetails.orderStatus =  OrderStatus(rawValue: String(cString: sqlite3_column_text(QueryStatement, 5)))!
            orderDetails.orderTotal =  Int(sqlite3_column_int(QueryStatement, 6))
            orderDetails.instructionToRestaurant =  String(cString: sqlite3_column_text(QueryStatement, 7))
            ordersDetails.append(orderDetails)
        }
        sqlite3_finalize(QueryStatement)
        
        return ordersDetails
    }
    
    func getpastOrderDetails(restaurantId : Int,offSet: Int, limit: Int)throws -> [OrderDetails]{
        
        var ordersDetails : [OrderDetails] = []
        let Query = "Select orders.order_id, user_id , restaurant_id ,user_address_id ,date_and_time,order_status , payment_amount,user_instruction_to_restaurant, star_rating , feedback FROM orders INNER JOIN track_order ON track_order.order_id = orders.order_id INNER JOIN payment_details ON payment_details.payment_id = orders.payment_id INNER JOIN user_instruction_to_restaurant ON user_instruction_to_restaurant.order_id = orders.order_id INNER JOIN food_rating ON food_rating.order_id = orders.order_id  WHERE  restaurant_id = ? and (order_status = 'Delivered' or order_status = 'Cancelled' or order_status = 'Declined') ORDER BY pk_order_id DESC LIMIT ? OFFSET ?;"
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_int(QueryStatement , 1, Int32(restaurantId)) == SQLITE_OK &&
                sqlite3_bind_int(QueryStatement , 2, Int32(limit)) == SQLITE_OK &&
                sqlite3_bind_int(QueryStatement , 3, Int32(offSet)) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            var orderDetails = OrderDetails()
            orderDetails.orderModel.orderId =  String(cString:  sqlite3_column_text(QueryStatement, 0))
            orderDetails.orderModel.userId =  Int(sqlite3_column_int(QueryStatement, 1))
            orderDetails.orderModel.restaurantId =  Int(sqlite3_column_int(QueryStatement, 2))
            orderDetails.orderModel.userAddressId =  String(cString: sqlite3_column_text(QueryStatement, 3))
            orderDetails.orderModel.dateAndTime =  String(cString: sqlite3_column_text(QueryStatement, 4))
            orderDetails.orderStatus =  OrderStatus(rawValue: String(cString: sqlite3_column_text(QueryStatement, 5)))!
            orderDetails.orderTotal =  Int(sqlite3_column_int(QueryStatement, 6))
            orderDetails.instructionToRestaurant =  String(cString: sqlite3_column_text(QueryStatement, 7))
            orderDetails.starRating =   Int(sqlite3_column_int(QueryStatement, 8))
            orderDetails.feedback =  String(cString: sqlite3_column_text(QueryStatement, 9))
            ordersDetails.append(orderDetails)
        }
        sqlite3_finalize(QueryStatement)
        
        return ordersDetails
    }
    
    func updateOrderCancellationReason(orderId : String , orderCancellationReason : String) throws -> Bool{
        
        var updated : Bool = false
        let query = "Update track_order SET order_cancellation_reason = ? where order_id = ? "
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_text(queryStatement , 1, ( orderCancellationReason as NSString).utf8String, -1, nil) == SQLITE_OK && sqlite3_bind_text(queryStatement , 2, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        if( sqlite3_step(queryStatement) == SQLITE_DONE){
            updated = true
        } else{
                throw SQLiteError.Step(message: errorMessage)
            }
        sqlite3_finalize(queryStatement)
        
        return updated
    }
    
    func getOrderCancellationReason(orderId : String) throws -> String{
        
        var orderCancellationReason = ""
        let query = "SELECT order_cancellation_reason FROM track_order where order_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_text(queryStatement , 1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
            while sqlite3_step(queryStatement) == SQLITE_ROW{
                orderCancellationReason =  String(cString:  sqlite3_column_text(queryStatement, 0))
            }
        
        sqlite3_finalize(queryStatement)
        
        return orderCancellationReason
    }
    
    func getUserDetails(orderId : String) throws -> UserDetails{
        
        var userDetails : UserDetails = UserDetails()
        let query = "SELECT user_name , user_phone_number FROM user_accounts INNER JOIN orders ON orders.user_id = user_accounts.user_id where order_id = ?;"
        let queryStatement : OpaquePointer? = try prepareStatement(query: query)
        guard sqlite3_bind_text(queryStatement , 1, ( orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else {
            throw SQLiteError.Bind(message: errorMessage)
        }
        if sqlite3_step(queryStatement) == SQLITE_ROW{
            userDetails.userName = String(cString: sqlite3_column_text(queryStatement, 0))
            userDetails.userPhoneNumber =  String(cString: sqlite3_column_text(queryStatement, 1))
            }
        
            sqlite3_finalize(queryStatement)
        
        return userDetails
    }
    
       
    
    func getBillDetailsOfOrder(orderId : String) throws -> BillDetailsModel{
        
        var billDetails : BillDetailsModel = BillDetailsModel()
        let Query = "Select item_total, delivery_fee ,restaurant_gst, restaurant_packaging_charges , discount_amount, total_cost FROM bill_details WHERE order_id = ?;"
        
        let QueryStatement : OpaquePointer? = try prepareStatement(query: Query)
        guard sqlite3_bind_text(QueryStatement , 1, (orderId as NSString).utf8String, -1, nil) == SQLITE_OK
        else{
                throw SQLiteError.Bind(message: errorMessage)
            }
        while(sqlite3_step(QueryStatement) == SQLITE_ROW){
            billDetails.itemTotal = 
            Int(sqlite3_column_int(QueryStatement, 0))
            
            billDetails.deliveryFee =  Int(sqlite3_column_int(QueryStatement, 1))
            billDetails.restaurantGST =  Double(sqlite3_column_double(QueryStatement, 2))
            billDetails.restaurantPackagingCharges =  Int(sqlite3_column_int(QueryStatement, 3))
            billDetails.itemDiscount =  Int(sqlite3_column_int(QueryStatement, 4))
            billDetails.totalCost =  Int(sqlite3_column_int(QueryStatement, 5))
            billDetails.orderId =  orderId
            
        }
       
        sqlite3_finalize(QueryStatement)
        
        return billDetails
    }
}
