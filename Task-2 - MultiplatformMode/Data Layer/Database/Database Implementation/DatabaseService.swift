//
//  Database.swift
//  Food Delivery Application
//
//  Created by Hrithik Kumar V on 02/12/21.
//

import Foundation
import SQLite3

class DatabaseService : DatabaseServiceContract{
   
    init(){
        DispatchQueue.global().async {
            if(self.createTables()){
                DispatchQueue.main.async {
                    return
                }
            }
        }
    }

    var errorMessage: String {
      if let error = sqlite3_errmsg(DatabaseConnection.getDatabaseConenction()) {
        let errorMessage = String(cString: error)
        return errorMessage
      } else {
        return "No error message provided from sqlite."
      }
    }
    
    func prepareStatement(query: String) throws -> OpaquePointer? {
       
        var statement: OpaquePointer?
        try enableForeignKeys(opaquePointer: DatabaseConnection.getDatabaseConenction())

        guard sqlite3_prepare_v2(DatabaseConnection.getDatabaseConenction(), query, -1, &statement, nil)
          == SQLITE_OK else {
        throw SQLiteError.Prepare(message: errorMessage)
      }
      return statement
     }
    
    func enableForeignKeys(opaquePointer : OpaquePointer?) throws{
        guard sqlite3_exec(opaquePointer,  "PRAGMA foreign_keys = on", nil, nil, nil) == SQLITE_OK else{
            throw SQLiteError.Execute(message: errorMessage)
        }
    }

    
}

enum SQLiteError: Error {
      case OpenDatabase(message: String)
      case Prepare(message: String)
      case Step(message: String)
      case Bind(message: String)
      case Execute(message : String)
}
