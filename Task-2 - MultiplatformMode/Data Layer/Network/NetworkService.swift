//
//  NetworkService.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation
class NetworkService : NetworkServiceContract{
    
    
    func getSecretCode(success: @escaping (_ code : String) -> Void , failure : @escaping (_ responseStatus : ResponseStatus) -> Void ){
        var datatask : URLSessionDataTask?
        
        let url = URL(string: "https://api.jsonbin.io/V3/b/61b9cb56229b23312cdbbfed/latest")
                var request = URLRequest(url: url!)
                let header = ["X-Master-Key": "$2b$10$OL9YNafdeNmHeq4c.c0Tme55ivkiFkhUFCxBzC9dXQkjmYZb9Pebu",
                              "X-Bin-Meta": "false"]
                request.allHTTPHeaderFields = header
                request.httpMethod = "GET"
         datatask = URLSession.shared.dataTask(with: request) { (data,response, error) in
            if data != nil {
                
                let status = self.getStatus(forError: error, andResponse: response)
                if(status == .success){
                    if let secretCode  = Parser.shared.parseSecretCode(data: data!){
                        success(secretCode)
                    }
                    else{
                        failure(.parserFailure)
                    }
                }
                else{
                    failure(status)
                }
            }
             else{
                 failure(.unknownError)
             }
        }
        datatask?.resume()
    }
    
    func getDetailsOfPincode( pincode : String ,success: @escaping (_ pincodeDetails : PincodeDetails) -> Void , failure : @escaping (_ responseStatus : ResponseStatus) -> Void ){
        let url = URL(string: "https://api.postalpincode.in/pincode/" + pincode)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        var datatask : URLSessionDataTask?
        datatask = URLSession.shared.dataTask(with: request) { (data,response, error) in
            if data != nil {
                
                let status  = self.getStatus(forError: error, andResponse: response)
                if(status == .success){
                    if let pincodeDetails = Parser.shared.parsePincodeData(data: data!){
                        success(pincodeDetails)
                    }
                    else{
                        failure(.parserFailure)
                    }
                }
                else{
                    failure(status)
                }
                
                    
            }
            else{
                failure(.unknownError)
            }
        }
        datatask?.resume()
        
    }
    
    func getCouponCode(couponCode : String, success: @escaping (_ couponCode : CouponCode) -> Void, failure : @escaping (_ responseStatus : ResponseStatus) -> Void){
        var datatask : URLSessionDataTask?
        let url = URL(string: "https://api.jsonbin.io/V3/b/622dd537a703bb67492a37e9/latest")
                var request = URLRequest(url: url!)
                let header = ["X-Master-Key": "$2b$10$OL9YNafdeNmHeq4c.c0Tme55ivkiFkhUFCxBzC9dXQkjmYZb9Pebu",
                              "X-Bin-Meta": "false"]
                request.allHTTPHeaderFields = header
                request.httpMethod = "GET"
        datatask = URLSession.shared.dataTask(with: request) { (data, response, error) in
        
            if(data != nil){
                let status = self.getStatus(forError: error, andResponse: response)
                if status == .success{
                    if let couponCodes = Parser.shared.parseCouponCode(data: data!){
                        var couponCodeDetails : CouponCode = CouponCode(couponCode: "", discountPercentage: 0, maximumDiscountAmount: 0)
                        for coupon in couponCodes{
                            if(coupon.couponCode == couponCode){
                                couponCodeDetails = coupon
                                break
                            }
                        }
                        success(couponCodeDetails)
                    }
                    else{
                        failure(.parserFailure)
                    }
                }
                else{
                    failure(status)
                }
        }
            else{
                failure(.unknownError)
            }
            
        datatask?.resume()
        
    }
    }
    
    func sendOTP(message: String , phoneNumber : String,complition : @escaping (_ response : Bool) -> Void){
        var dataTask : URLSessionDataTask?
      
        let url = URL(string: "https://www.fast2sms.com/dev/bulkV2")
        var request = URLRequest(url: url!)
        let header = ["authorization": "2krxTyjFlWGc6uNZAEiDvVliziEtYHrWXVjxtfTp3uvixoGZdipTddNLZhRq"]
        request.allHTTPHeaderFields = header
        request.httpBody = "variables_values=\(message)&route=otp&numbers=\(phoneNumber)".data(using: String.Encoding.utf8)
        request.httpMethod = "POST"
        dataTask = URLSession.shared.dataTask(with: request) { (data,response, error) in
            if error == nil && data != nil {
                do {
                    let dic  = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String : Any]
                    print(dic as Any)
                    complition(true)
                }
                catch{
                    print("Error parsing json")
                }
            }
        }
        dataTask?.resume()
        
        print(message,phoneNumber)

    }
    
    private func getStatus(forError error: Error?, andResponse response: URLResponse?) -> ResponseStatus {
    
        if error == nil {
            let code = (response as! HTTPURLResponse).statusCode
            switch code {
            case 401:
                return .authenticationFailure
            case 200:
                return .success
            default:
                return .unknownError
            }
        } else {
            switch (error! as NSError).code {
            case NSURLErrorTimedOut:
                return .timeout
            case NSURLErrorNotConnectedToInternet:
                return .networkUnavailable
            
            default:
                return .unknownError
            }
        }
    }
}


protocol NetworkServiceContract : RestaurantAccountNetworkServiceContract,AddressNetworkServiceContract,OTPNetworkServiceContract,CartNetworkServiceContract{
 
}

protocol RestaurantAccountNetworkServiceContract{
    func getSecretCode(success: @escaping (_ code : String) -> Void , failure : @escaping (_ responseStatus : ResponseStatus) -> Void )
}

protocol AddressNetworkServiceContract{
    func getDetailsOfPincode( pincode : String ,success: @escaping (_ pincodeDetails : PincodeDetails) -> Void , failure : @escaping (_ responseStatus : ResponseStatus) -> Void )
    
}

protocol OTPNetworkServiceContract{
    func sendOTP(message: String , phoneNumber : String,complition : @escaping (_ response : Bool) -> Void)
}

protocol CartNetworkServiceContract{
    func getCouponCode(couponCode : String, success: @escaping (_ couponCode : CouponCode) -> Void, failure : @escaping (_ responseStatus : ResponseStatus) -> Void)
}
