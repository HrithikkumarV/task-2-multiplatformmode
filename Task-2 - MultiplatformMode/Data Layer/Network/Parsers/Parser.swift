//
//  PincodeDetailsParser.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 27/06/22.
//

import Foundation

class Parser{
    
    static let shared  = Parser()
    
    func parsePincodeData(data: Data) -> PincodeDetails?{
       
        let postDetails  = try? (JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [[String : Any]])
        var pincodeDetails : PincodeDetails = PincodeDetails()
        if postDetails?[0]["Status"] as! String != "Error" {
            for postDetail in postDetails?[0]["PostOffice"] as? NSArray ?? []{
                let post = postDetail as? Dictionary<String, Any>
                pincodeDetails.country =  post?["Country"]  as? String ?? ""
                pincodeDetails.state =  post?["State"] as? String ?? ""
                pincodeDetails.city =  post?["Division"] as? String ?? ""
                pincodeDetails.pincode =  post?["Pincode"] as? String ?? ""
                pincodeDetails.localities.append(post?["Name"] as? String ?? "")
            }
            return pincodeDetails
        }
        return nil
        
    }
    
    func parseCouponCode(data : Data) -> [CouponCode]?{
        let object = try? JSONSerialization.jsonObject(with:
                               data, options: [])
        if(object != nil){
            let decoder = JSONDecoder()
            guard let couponCodes =  try? decoder.decode([CouponCode].self, from: data)
            else{
                 return nil
            }
            return couponCodes
        }
        return nil
    }
    
    func parseSecretCode(data : Data) -> String? {
        let object  = try? (JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : String] ?? ["":""])
        return object?["secretCode"]
    }
}
