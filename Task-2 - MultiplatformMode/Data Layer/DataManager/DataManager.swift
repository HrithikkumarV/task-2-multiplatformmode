//
//  DataManager.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation

enum ServerType {
    case local
    case network
}

class DataManager : DataManagerContracts{
   
    var networkService : NetworkServiceContract
    
    var databaseService : RestaurantsDAOContracts
    
    init(databaseService : RestaurantsDAOContracts , networkService : NetworkServiceContract){
        self.databaseService = databaseService
        self.networkService = networkService
    }
    
    func getListOfRestaurentDetails(withParameter: getRestaurantsDetailsParameter, success: ( _ listOfRestaurantDetails :[RestaurantCompleteDetails]?) -> Void, failure : @escaping (_ responseStatus : ResponseStatus) -> Void){
        
        if(withParameter.requestType == .local){
            do {
                let restaurantCompleteDetails = try databaseService.getRestaurentsTabContentDetails(locality: withParameter.locality, pincode: withParameter.pincode)
                    success(restaurantCompleteDetails)
              
            }
            catch{
                print(error)
                failure(.databaseFailure)
            }
            
        }
    }
    
    
    func GetPincodeDetails(withParameter: GetPincodeDetailsParameter, success: @escaping ( _ pincodeDetails :PincodeDetails) -> Void, failure : @escaping (_ responseStatus : ResponseStatus) -> Void){
        if(withParameter.requestType == .server){
            networkService.getDetailsOfPincode(pincode: withParameter.pincode) { pincodeDetails in
                success(pincodeDetails)
            } failure: { responseStatus in
                failure(responseStatus)
            }

        }
    }
}






