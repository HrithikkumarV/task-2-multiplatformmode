//
//  MultiplatformModeControllerType2.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Cocoa

class MultiPlatformModeSplitViewControllerType2 : NSSplitViewController{
    
   

    let toolBar : NSToolbar = {
       let toolbar = NSToolbar(identifier: .init("Default"))
        toolbar.allowsUserCustomization = true
        toolbar.displayMode = .default
       return toolbar
    }()
   
    var backArrowToolbarItem : NSToolbarItem?
    
    var wantBackToolBarButton : Bool = false
  
    private(set) var mode : Modes?{
        didSet{
                
                switch mode{
                    case .Mobile:
                        mobileMode()
                  
                        break
                    case .Tab:
                        tabMode()
                        break
                    case .Desktop:
                        desktopMode()
                        break
                
                    case .none:
                        break
                    }
            
            }
        
        }
    
    
    var sideBarViewController : NSViewController?
    
    var contentListViewController : NSViewController?
    
    var detailViewController : NSViewController?
    
    var sidebarSplitItem : NSSplitViewItem?
        
    var contentListSplitViewItem : NSSplitViewItem?
        
    var detailViewSplitItem : NSSplitViewItem?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        splitView.dividerStyle = .thin
    }
    
    override func viewDidAppear() {
        if(wantBackToolBarButton){
            toolBar.delegate = self
            self.view.window?.toolbar = toolBar
        }
    }
        
    override func splitViewWillResizeSubviews(_ notification: Notification){
        findMode()
    }
    
    
    
    
    func addSideBarViewController(viewController : NSViewController){
        if(sidebarSplitItem == nil){
            sideBarViewController = viewController
                sidebarSplitItem = NSSplitViewItem(sidebarWithViewController: viewController)
                sidebarSplitItem?.minimumThickness = 300
                sidebarSplitItem?.canCollapse = false
                mode = nil
                addSplitViewItem(sidebarSplitItem!)
        }
    }
    
    func addContentListViewController(viewController : NSViewController){
        if(contentListSplitViewItem == nil){
            contentListViewController = viewController
            contentListSplitViewItem =  NSSplitViewItem(contentListWithViewController: viewController)
            contentListSplitViewItem?.minimumThickness = 300
            mode = nil
            addSplitViewItem(contentListSplitViewItem!)
        }
       
    }
    
    func addDetailViewController(viewController : NSViewController){
        if(detailViewSplitItem == nil){
            detailViewController = viewController
            detailViewSplitItem = NSSplitViewItem(viewController: viewController)
            detailViewSplitItem?.minimumThickness = 300
            mode = nil
            addSplitViewItem(detailViewSplitItem!)
        }
       
    }
    
    func didTapBackButton(){
            
                
        if(detailViewController != nil){
                contentListSplitViewItem?.isCollapsed = false
                removeDetailSplitViewItem()
                    
        }
        else if(contentListViewController != nil){
                sidebarSplitItem?.isCollapsed = false
                removeContentListSplitViewItem()
        }
                
    }
    private func findMode(){
        if((splitView.frame.size.width > 1000)  && (mode != .Desktop || mode == nil)){
            
            mode = .Desktop
            
        }
        else if((splitView.frame.size.width <= 1000 && splitView.frame.size.width > 700) && (mode != .Tab || mode == nil)){
            
            mode = .Tab
           
        }
        else if((mode != .Mobile || mode == nil) && splitView.frame.size.width <= 700){
           
            mode = .Mobile
        }
    }
        
       
    private func desktopMode(){
        self.view.window?.title = "Desktop Mode"
        if(sideBarViewController != nil){
            sidebarSplitItem?.isCollapsed = false
            
        }
        if(contentListViewController != nil){
            contentListSplitViewItem?.isCollapsed = false
            
           
        }
        if(detailViewController != nil){
            detailViewSplitItem?.isCollapsed = false
            
        }
        
    }
    
    private func tabMode(){
        self.view.window?.title = "Tab Mode"
        if(sideBarViewController != nil){
            sidebarSplitItem?.isCollapsed = false
        }
        if(detailViewController != nil && contentListViewController != nil){
            detailViewSplitItem?.isCollapsed = false
            contentListSplitViewItem?.isCollapsed = true
           
        }
        else if(contentListViewController != nil){
            contentListSplitViewItem?.isCollapsed = false
            
        }
        
    }
    
    private func mobileMode(){
        self.view.window?.title = "Mobile Mode"
        if(detailViewController != nil){
            detailViewSplitItem?.isCollapsed = false
            contentListSplitViewItem?.isCollapsed = true
            sidebarSplitItem?.isCollapsed = true
            
        }
        else if(contentListViewController != nil){
            detailViewSplitItem?.isCollapsed = true
            contentListSplitViewItem?.isCollapsed = false
            sidebarSplitItem?.isCollapsed = true
            
        }
        else if(sideBarViewController != nil){
            detailViewSplitItem?.isCollapsed = true
            contentListSplitViewItem?.isCollapsed = true
            sidebarSplitItem?.isCollapsed = false
            
        }
        else{
            
        }
    }

    
    private func removeSideBarSplitViewItem(){
        removeSplitViewItem(sidebarSplitItem!)
        sidebarSplitItem = nil
        sideBarViewController = nil

    }
    
    private func removeContentListSplitViewItem(){
        removeSplitViewItem(contentListSplitViewItem!)
        contentListSplitViewItem = nil
        contentListViewController = nil
        
    }
    
    private func removeDetailSplitViewItem(){
        removeSplitViewItem(detailViewSplitItem!)
        detailViewSplitItem = nil
        detailViewController = nil
    }
    
    

    
    
    enum Modes {
        case Mobile
        case Tab
        case Desktop
        
    }
    
    deinit{
        self.view.window?.toolbar = nil
    }
    
  
}



extension MultiPlatformModeSplitViewControllerType2 : NSToolbarDelegate {
    
    
    
    func toolbar(_ toolbar: NSToolbar, itemForItemIdentifier itemIdentifier: NSToolbarItem.Identifier, willBeInsertedIntoToolbar flag: Bool) -> NSToolbarItem? {
        
        if itemIdentifier == NSToolbarItem.Identifier.backwardArrow {
            backArrowToolbarItem = customToolbarItem(itemForItemIdentifier: NSToolbarItem.Identifier.backwardArrow.rawValue,
                                                     label: NSLocalizedString("Back", comment: ""),
                                                     paletteLabel: NSLocalizedString("Back", comment: ""),
                                                     toolTip: NSLocalizedString("Go To Previous Page", comment: ""),
                                                     iconImage: NSImage(systemSymbolName: "arrow.backward", accessibilityDescription: "Backward  Arrow")!)!
            return backArrowToolbarItem!
                
        }
        else {
            return nil
        }
    }
    func toolbarAllowedItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return [.backwardArrow]
    }
    func toolbarDefaultItemIdentifiers(_ toolbar: NSToolbar) -> [NSToolbarItem.Identifier] {
        return [.backwardArrow]
    }
    
    func toolbarWillAddItem(_ notification: Notification) {
        
    }
    
    func toolbarDidRemoveItem(_ notification: Notification) {
        
    }
    
    

    func customToolbarItem(
        itemForItemIdentifier itemIdentifier: String,
        label: String,
        paletteLabel: String,
        toolTip: String,
        iconImage: NSImage ) -> NSToolbarItem? {
        
        let toolbarItem = NSToolbarItem(itemIdentifier: NSToolbarItem.Identifier(rawValue: itemIdentifier))
        toolbarItem.label = label
        toolbarItem.paletteLabel = paletteLabel
        toolbarItem.toolTip = toolTip
        toolbarItem.target = self
        toolbarItem.image = iconImage
        toolbarItem.action = #selector(didTapToolBarItem(sender:))
        toolbarItem.isNavigational = true
        toolbarItem.isBordered = true
        
        let menuItem: NSMenuItem = NSMenuItem()
        menuItem.submenu = nil
        menuItem.title = label
        menuItem.target = self
        menuItem.action = #selector(didTapMenuItem(sender:))
        toolbarItem.menuFormRepresentation = menuItem
        
        return toolbarItem
    }
    
    @objc func didTapToolBarItem(sender : NSToolbarItem){
        self.didTapBackButton()
    }
    
    @objc func didTapMenuItem(sender : NSMenuItem){
        self.didTapBackButton()
    }
}




