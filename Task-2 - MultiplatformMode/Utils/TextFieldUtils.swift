//
//  TextFieldUtils.swift
//  Foodzy
//
//  Created by Hrithik Kumar V on 27/05/22.
//

import Cocoa
//extension NSTextField {
//
//private func setPasswordToggleImage(_ button: NSButton) {
//    if(self.pass){
//        button.setImage(NSImage(systemName: "eye.slash")?.withTintColor(.black.withAlphaComponent(0.9), renderingMode: .alwaysOriginal), for: .normal)
//    }
//    else{
//        button.setImage(NSImage(systemName: "eye")?.withTintColor(.black.withAlphaComponent(0.9), renderingMode: .alwaysOriginal), for: .normal)
//
//    }
//}
//
//func enablePasswordToggle(){
//    let button = NSButton()
//    setPasswordToggleImage(button)
//    button.imageEdgeInsets = NSEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
//    button.frame = CGRect(x: self.frame.size.width - 25, y: 5, width: 25, height: 25)
//    button.addTarget(self, action: #selector(self.togglePasswordView), for: .touchUpInside)
//    self.rightView = button
//    self.righ
//}
//    
//@objc func togglePasswordView(_ sender: Any) {
//    
//    self.isSecureTextEntry = !self.isSecureTextEntry
//    setPasswordToggleImage(sender as! NSButton)
//}
//}
//
//
//
//extension UITextField{
//    
//    func addLabelToTopBorder(labelText : String,fontSize : CGFloat){
//        let topBorderLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width - 20, height: fontSize))
//        self.addBorder(side: .top, color: .systemGray5, width: 1)
//        self.addBorder(side: .right, color: .systemGray5, width: 1)
//        self.addBorder(side: .left, color: .systemGray5, width: 1)
//        self.addBorder(side: .bottom, color: .systemGray5, width: 1)
//        topBorderLabel.text = labelText
//        topBorderLabel.textColor = .lightGray
//        topBorderLabel.backgroundColor = .white
//        self.addSubview(topBorderLabel)
//        topBorderLabel.translatesAutoresizingMaskIntoConstraints = false
//        topBorderLabel.centerYAnchor.constraint(equalTo: self.topAnchor).isActive = true
//        topBorderLabel.leftAnchor.constraint(equalTo: self.leftAnchor,constant: 15).isActive = true
//        topBorderLabel.tag = 111
//        topBorderLabel.isHidden = true
//        self.addTarget(self, action: #selector(TextFieldDidChange) , for:.editingChanged)
//    }
//    
//    @objc func TextFieldDidChange(){
//        if self.text!.isEmpty{
//            hideTopBorderLabel()
//        }
//        else{
//            showTopBorderLabel()
//        }
//    }
//    
//    func hideTopBorderLabel(){
//        self.viewWithTag(111)?.isHidden = true
//    }
//    
//    func showTopBorderLabel(){
//        self.viewWithTag(111)?.isHidden = false
//    }
//    
//     
//}
//
//extension UITextField{
//    func addRightImage(image : UIImage){
//        let rightImage = UIImageView(frame: CGRect(x: self.frame.size.width - 35 , y: 0, width: 25, height: 25))
//        self.rightView = rightImage
//        self.rightViewMode = .always
//        
//    }
//    
//    func addLeftImage(image : UIImage){
//        let leftImage = UIImageView(frame: CGRect(x: 15, y: 0, width: 25, height: 25))
//        self.leftView = leftImage
//        self.leftViewMode = .always
//        
//    }
//    
//}
//
//extension UITextField{
//    func enableClearText(){
//        let button = UIButton(type: .custom)
//        button.setImage(UIImage(systemName: "multiply")?.withTintColor(.black, renderingMode: .alwaysOriginal), for: .normal)
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: 0)
//        button.frame = CGRect(x: self.frame.size.width - 25, y: 5, width: 25, height: 25)
//        button.addTarget(self, action: #selector(self.clearTextInTextField(sender:)), for: .touchUpInside)
//        button.isHidden = true
//        self.addTarget(self, action: #selector(textFieldValueChanged) , for:.editingChanged)
//        self.rightView = button
//        self.rightViewMode = .always
//    }
//    
//    @objc func textFieldValueChanged(){
//        if self.text!.isEmpty{
//            hideCrossSymbol()
//        }
//        else{
//            showCrossSymbol()
//        }
//    }
//    
//    func hideCrossSymbol(){
//        for subview in self.subviews{
//                subview.isHidden = true
//        }
//    }
//    
//    func showCrossSymbol(){
//        for subview in self.subviews{
//                subview.isHidden = false
//        }
//    }
//        
//    @objc func clearTextInTextField(sender : UIButton) {
//        self.text = ""
//        sender.isHidden = true
//        
//    }
//}
//
//class CustomTextField: UITextField {
//    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
//    
//    override open func textRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//
//    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
//        return bounds.inset(by: padding)
//    }
//}
//
