//
//  AppDelegate.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//


import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

   
    private var windowController : NSWindowController!
    
    private var router : MultiPlatformModeRouterContract?
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        
        openMainWindow()
        
        
       
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        DispatchQueue.global().async {
            DatabaseConnection.closeDatabaseConnection()
        }
       
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }

    private func openMainWindow(){
        router = MultiPlatformModeRouter()
        windowController = router?.mainWindowController
        router?.launch()
        
    }
   
    func applicationShouldHandleReopen(_ sender: NSApplication, hasVisibleWindows flag: Bool) -> Bool {
        if !flag{
            windowController.window?.makeKeyAndOrderFront(nil)
            windowController.window?.makeMain()
        }
        return true
    }
    
    
    
}

