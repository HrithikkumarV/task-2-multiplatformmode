//
//  MainScreenPresenter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class MainScreenPresenter : MainScreenPresenterContract{
    
    weak var router : MainScreenRouterContract?
    weak var viewController : MainViewControllerContract?
    
   
    
    func didTapUserButton() {

        router?.addSearchTable()
        
    }
    
    func didTapRestaurantButton() {

        router?.addSearchTable()
    }
}

protocol MainScreenPresenterContract : AnyObject{
    var router : MainScreenRouterContract? {get set}
    var viewController : MainViewControllerContract? {get set}
    func didTapUserButton()
    func didTapRestaurantButton()
}
