//
//  MainScreenAssembler.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation
class MainScreenAssembler{
    static func getBaseViewController(router : MainScreenRouterContract) -> MainViewController{
        let viewController = MainViewController()
        let presenter = MainScreenPresenter()
        presenter.router = router
        presenter.viewController = viewController
        viewController.presenter = presenter
        return viewController
    }
    
    
}
