//
//  MainWindowRouterContract.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

protocol MainWindowRouterContract : AnyObject{
    var mainWindowController :  MainWindowControllerContract? {get}

}
