//
//  MultiPlatFormModeRouter.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa

class MultiPlatformModeRouter : MultiPlatformModeRouterContract{
   
    
    
    lazy var mainWindowController: MainWindowControllerContract? = {
        let windowConroller = MainWindowController()
        windowConroller.router = self
        windowConroller.contentViewController = baseViewController
        return windowConroller
    }()

    var baseViewController  : MultiplatformModeControllerContract?

    var mainScreenBaseViewController: MainViewControllerContract?
    
    var searchTableBaseViewController: SearchTableViewControllerContract?
   
    var displayRestaurantsbaseViewController: DisplayRestaurantsCollectionViewControllerContract?
    
    init(){
        baseViewController = MultiPlatformModeAssembler.getBaseViewController(router: self)
    }
    func launch(){
        addMainScreen()
    }
    
  
    func addMainScreen() {
        mainScreenBaseViewController = MainScreenAssembler.getBaseViewController(router: self)
        baseViewController?.presenter?.addSideBarViewController(viewController: mainScreenBaseViewController!)
    }
    
    func addSearchTable() {
        searchTableBaseViewController = SearchTableAssembler.getBaseViewController(router: self)
        baseViewController?.presenter?.addContentListViewController(viewController: searchTableBaseViewController!)
    }
    
    func addDisplayRestaurants() {
        displayRestaurantsbaseViewController = DisplayRestaurantsAssembler.getBaseViewController(router: self)
        baseViewController?.presenter?.addDetailViewController(viewController: displayRestaurantsbaseViewController!)
    }
    
    
    
}

protocol MultiPlatformModeRouterContract : DisplayRestaurantsRouterContract,SearchTableRouterContract,MainScreenRouterContract,MainWindowRouterContract{
    var baseViewController  : MultiplatformModeControllerContract? {get set}
    func launch()
}





