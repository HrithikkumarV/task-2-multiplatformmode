//
//  MainScreenUI.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//


import Cocoa

class MainViewController : NSViewController, MainViewDelegate , MainViewControllerContract{
    
    var presenter : MainScreenPresenterContract?
   
    private let visualEffect : NSVisualEffectView = {
        let visualEffect = NSVisualEffectView()
        visualEffect.blendingMode = NSVisualEffectView.BlendingMode.behindWindow
        visualEffect.material = NSVisualEffectView.Material.underWindowBackground
        visualEffect.translatesAutoresizingMaskIntoConstraints = false
        return visualEffect
    }()
    
    private let mainView : MainView = {
        let mainView = MainView()
        mainView.layer?.cornerRadius = 10
        mainView.translatesAutoresizingMaskIntoConstraints = false
        return mainView
    }()
    
   
    
    override func loadView() {
        view = NSView()
        addVisualEffect()
        addMainView()
        mainView.delegate = self
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
       
      
    }
    
    
    private func addVisualEffect() {
        view.addSubview(visualEffect, positioned: .below, relativeTo: nil )
        NSLayoutConstraint.activate([visualEffect.topAnchor.constraint(equalTo: view.topAnchor),visualEffect.leftAnchor.constraint(equalTo: view.leftAnchor),visualEffect.rightAnchor.constraint(equalTo: view.rightAnchor),visualEffect.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    
    private func addMainView() {
        view.addSubview(mainView)
        NSLayoutConstraint.activate([mainView.topAnchor.constraint(equalTo: view.topAnchor,constant: 5),mainView.leftAnchor.constraint(equalTo: view.leftAnchor,constant: 5),mainView.rightAnchor.constraint(equalTo: view.rightAnchor,constant: -5),mainView.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -5)])
    }
    
    func didTapUserButton() {
        presenter?.didTapUserButton()
    }
    
    func didTapRestaurantButton() {
        presenter?.didTapRestaurantButton()
    }
    
    

}


protocol MainViewControllerContract : NSViewController{
    var presenter : MainScreenPresenterContract? {get set}
}
