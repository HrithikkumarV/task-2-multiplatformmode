//
//  DisplayRestaurantUI.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//


import Cocoa


class DisplayRestaurantsCollectionViewController  : NSViewController, NSCollectionViewDelegateFlowLayout, NSCollectionViewDataSource , NSCollectionViewDelegate,DisplayRestaurantsCollectionViewControllerContract{
    
   
    var presenter : DisplayRestaurantsPresenterContract?
    
    let cachedCollectionViewItems = [DisplayRestaurantsCollectionViewItem.cellIdentifier : DisplayRestaurantsCollectionViewItem()]
    
    let pasteBoard = NSPasteboard.PasteboardType("CollectionView.item")
    
    var listOfRestaurantDetails : [RestaurantCompleteDetails] = []
    
    private let collectionView : NSCollectionView = {
        let collectionView =  NSCollectionView()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.wantsLayer = true
        collectionView.isSelectable = true
        collectionView.allowsEmptySelection = true
        collectionView.allowsMultipleSelection = true
        collectionView.enclosingScrollView?.borderType = .noBorder
        return collectionView
    }()
    
    private let scrollView : NSScrollView = {
        let scrollView = NSScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    
    
   
    
    override func loadView() {
        view = NSView()
        view.wantsLayer = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getListOfRestaurantDetails()
        addScrollView()
        configureCollectionView()
        addCachedItemsAsCollctionViewSubview()
       
    }
    
    
    func getListOfRestaurantDetails(){
        presenter?.getListOfRestaurantDetails(locality: "", pincode: "")
    }
    
    func updateListOfRestaurantDetailsInView(listOfRestaurantDetails : [RestaurantCompleteDetails]){
        self.listOfRestaurantDetails = listOfRestaurantDetails
        collectionView.reloadData()
    }
   
    private func configureCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerForDraggedTypes([pasteBoard])
        collectionView.setDraggingSourceOperationMask(.move, forLocal: true)
        configureFlowLayout()
//        configureGridLayout()
        collectionView.register(DisplayRestaurantsCollectionViewItem.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: DisplayRestaurantsCollectionViewItem.cellIdentifier))
        scrollView.documentView =  collectionView
    }
    
    private func configureFlowLayout() {
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = 30.0
        flowLayout.minimumLineSpacing = 30.0
        flowLayout.sectionInset = NSEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = NSSize(width: 200, height: 250)
        collectionView.collectionViewLayout = flowLayout
    }
    
    private func configureGridLayout() {
        let gridLayout = NSCollectionViewGridLayout()
        gridLayout.maximumNumberOfRows = 5
        gridLayout.maximumNumberOfColumns = 5
        gridLayout.minimumInteritemSpacing = 30.0
        gridLayout.minimumLineSpacing = 30.0
        gridLayout.minimumItemSize = NSSize(width: 200, height: 250)
//        gridLayout.maximumItemSize = NSSize(width: 200, height: 250)
        collectionView.collectionViewLayout = gridLayout
    }
  

    
    private func addScrollView() {
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([scrollView.topAnchor.constraint(equalTo: view.topAnchor),scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    
    }
    
    private func addCachedItemsAsCollctionViewSubview(){
        for item in  cachedCollectionViewItems.values{
            collectionView.addSubview(item.view)
            item.view.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                item.view.topAnchor.constraint(equalTo: collectionView.topAnchor),
                item.view.leftAnchor.constraint(equalTo: collectionView.leftAnchor),
                item.view.widthAnchor.constraint(equalToConstant: 200)
            ])
        }
    }
    

    
    
}


extension DisplayRestaurantsCollectionViewController{
    
    
    func numberOfSections(in collectionView: NSCollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        listOfRestaurantDetails.count
       
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
    
        let restaurant = listOfRestaurantDetails[indexPath.item]
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(DisplayRestaurantsCollectionViewItem.cellIdentifier), for: indexPath) as? DisplayRestaurantsCollectionViewItem
        item?.restaurantDisplayCollectionViewItemView.configureContent(displayRestaurantItemViewData: DisplayRestaurentItemViewModel(restaurantProfileImage: restaurant.restaurant.restaurantProfileImage , restaurantName: restaurant.restaurant.restaurantName, restaurantStarRating: "⭐ \(restaurant.restaurantStarRating)", restaurantCuisine: restaurant.restaurant.restaurantCuisine, restaurantLocality: restaurant.restaurantAddress.landmark, deliveryTiming: 30, restaurantOpensNextAt: restaurant.restaurantOpensNextAt, restaurantIsAvailable: restaurant.restaurantIsAvailable, isDeliverable: true))
        return item ?? NSCollectionViewItem()
    }
  

    
    func collectionView(_ collectionView: NSCollectionView, canDragItemsAt indexPaths: Set<IndexPath>, with event: NSEvent) -> Bool {

        return true
    }
    
    func collectionView(_ collectionView: NSCollectionView, pasteboardWriterForItemAt indexPath: IndexPath) -> NSPasteboardWriting? {
        let pasteboardItem = NSPasteboardItem()
        pasteboardItem.setString("\(indexPath.item)", forType: pasteBoard)
        print(indexPath.item)
        return pasteboardItem
    }

    func collectionView(_ collectionView: NSCollectionView, validateDrop draggingInfo: NSDraggingInfo, proposedIndexPath proposedDropIndexPath: AutoreleasingUnsafeMutablePointer<NSIndexPath>, dropOperation proposedDropOperation: UnsafeMutablePointer<NSCollectionView.DropOperation>) -> NSDragOperation {
        return .move
    }

    func collectionView(_ collectionView: NSCollectionView, acceptDrop draggingInfo: NSDraggingInfo, indexPath: IndexPath, dropOperation: NSCollectionView.DropOperation) -> Bool {
        guard let item = draggingInfo.draggingPasteboard.pasteboardItems?.first,
                      let originalRow = Int(item.string(forType: pasteBoard)!)
                      else { return false }

        print(originalRow,indexPath)

        
        collectionView.animator().moveItem(at: IndexPath(item: originalRow, section: 0), to: indexPath)
        collectionView.draggingUpdated(draggingInfo)
        
        return true
    }
    
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        
    }
    
    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
        
    }
    
   
    
}


protocol DisplayRestaurantsCollectionViewControllerContract : NSViewController{
    var presenter : DisplayRestaurantsPresenterContract? {get set}
    func updateListOfRestaurantDetailsInView(listOfRestaurantDetails : [RestaurantCompleteDetails])
}
