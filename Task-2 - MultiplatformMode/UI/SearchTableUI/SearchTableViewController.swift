//
//  OutlineViewUI.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Cocoa


class SearchTableViewController: NSViewController,SearchSuggestionTableSectionHeaderViewDelegate, NSPopoverDelegate,SearchTableViewControllerContract, NSSearchFieldDelegate{
   
    var presenter : SearchTablePresenterContract?
    
    var theme : Theme = Theme()
    
    
   
    private let searchTypes : [SearchType] = [.searchHistoryItem , .searchSuggestionItem]
   
    
    private let searchItems : [SearchType : [Search]] = [.searchHistoryItem : [Search(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , Search(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),Search(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),Search(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , Search(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),Search(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),Search(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , Search(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),Search(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),Search(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , Search(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),Search(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),Search(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , Search(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),Search(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem),Search(searchItemName: "a", searchItemType: .Menu, searchType: .searchHistoryItem) , Search(searchItemName: "b", searchItemType: .Restaurant, searchType: .searchHistoryItem),Search(searchItemName: "c", searchItemType: .Search, searchType: .searchHistoryItem)] , .searchSuggestionItem : [Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem),Search(searchItemName: "A", searchItemType: .Menu, searchType: .searchSuggestionItem) , Search(searchItemName: "B", searchItemType: .Restaurant, searchType: .searchSuggestionItem),Search(searchItemName: "C", searchItemType: .Search, searchType: .searchSuggestionItem)]]
    

    private var dataForTable : [(sectionType : TableViewSectionType, data : Any)] = []
    
    private let cachedTableViewCells = [SearchSuggestionTableSectionHeaderViewCell.cellIdentifier : SearchSuggestionTableSectionHeaderViewCell(), SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier :  SearchSuggestionAndSearchHistoryTableViewCell()]
    
    private let pasteBoard = NSPasteboard.PasteboardType(rawValue: "SearchFood.search")

    
    
    private let searchBar : NSSearchField = {
        let searchBar = NSSearchField()
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.wantsLayer = true
        searchBar.focusRingType = .none
        return searchBar
        
    }()
    
    private let popOver : NSPopover = {
        let popOver = NSPopover()
        return popOver
    }()
    

    private let tableView : NSTableView = {
        let tableView = NSTableView()
        tableView.intercellSpacing = NSSize(width: 0, height: 2)
        tableView.selectionHighlightStyle = .regular
        tableView.style = .plain
        tableView.allowsColumnResizing = true
        tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        return tableView
    }()
    
    private var tableHeight : CGFloat = 0
   
    
    private let tableColumn1 : NSTableColumn = {
        let tableColumn = NSTableColumn()

        return tableColumn
    }()

    
    private let scrollView : NSScrollView = {
        let scrollView = NSScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
  
    
    private let visualEffect : NSVisualEffectView = {
        let visualEffect = NSVisualEffectView()
        visualEffect.blendingMode = NSVisualEffectView.BlendingMode.behindWindow
        visualEffect.material = NSVisualEffectView.Material.underWindowBackground
        visualEffect.translatesAutoresizingMaskIntoConstraints = false
        return visualEffect
    }()
    

    
   
    
   
    override func loadView() {
        self.view = NSView()
        addVisualEffect()
        addSearchBar()
        addScrollView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyTheme()
        prepareDataForTable()
        addTableViewProperties()
        addScrollViewProperties()
        
    }
    
   
    override func viewDidAppear() {
        super.viewDidAppear()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
   
    
    
    func searchFieldDidEndSearching(_ sender: NSSearchField) {
        
    }
    
    func searchFieldDidStartSearching(_ sender: NSSearchField) {
        //print(sender.stringValue)
        
    }
    
    func control(_ control: NSControl, textShouldEndEditing fieldEditor: NSText) -> Bool {
        //print(fieldEditor.string)
        
        presenter?.getPicodeDetails(pincode: fieldEditor.string)

        return true
    }
    
    
    
    func updatePincodeDetailsInSearchBar(PincodeDetails : PincodeDetails){
        self.searchBar.stringValue = PincodeDetails.city
    }
    
    func addScrollViewProperties(){
        scrollView.documentView = tableView
    }
    
    
    
    func addTableViewProperties(){
        tableView.headerView = nil
        tableView.registerForDraggedTypes([pasteBoard])
        tableView.addTableColumn(tableColumn1)
        addCachedCellsAsTableViewSubview()
       
    }
    
    
    
    private func prepareDataForTable(){
        var index = 0
        for searchType in searchTypes{
            dataForTable.append((sectionType: .sectionHeader, data: searchType))
            for searchItem in searchItems[searchType] ?? []{
                index += 1
                dataForTable.append((sectionType: .sectionRow, data: searchItem))
            }
            index += 1
        }
       
    }
   
    
    private func applyTheme(){
        searchBar.layer?.backgroundColor = theme.searchBarBackgroundColor
        searchBar.textColor = theme.searchBarTextColor
        searchBar.font = theme.searchBarFont
        tableView.backgroundColor = theme.tableViewBackgroundColor
        scrollView.backgroundColor = theme.scrollViewBackgroundColor
        view.layer?.backgroundColor = theme.viewBackgroundColor
    }

    private func addScrollView() {
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([scrollView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    
    }
    
    private func addVisualEffect() {
        view.addSubview(visualEffect, positioned: .below, relativeTo: nil )
        NSLayoutConstraint.activate([visualEffect.topAnchor.constraint(equalTo: view.topAnchor),visualEffect.leftAnchor.constraint(equalTo: view.leftAnchor),visualEffect.rightAnchor.constraint(equalTo: view.rightAnchor),visualEffect.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    
    }
    
    
    private func addSearchBar(){
        view.addSubview(searchBar)
        searchBar.delegate =  self
        NSLayoutConstraint.activate([searchBar.topAnchor.constraint(equalTo: view.topAnchor),searchBar.rightAnchor.constraint(equalTo: view.rightAnchor),searchBar.leftAnchor.constraint(equalTo: view.leftAnchor),searchBar.heightAnchor.constraint(equalToConstant: 50)])
        
    }
    
    private func addCachedCellsAsTableViewSubview(){
        for cell in  cachedTableViewCells.values{
            tableView.addSubview(cell)
            cell.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                cell.topAnchor.constraint(equalTo: tableView.topAnchor),
                cell.leftAnchor.constraint(equalTo: tableView.leftAnchor),
                cell.rightAnchor.constraint(equalTo: tableView.rightAnchor)
            ])
        }
    }
    
    
    
    
    func didTapClearHistory(sender : NSButton) {
        presenter?.didTapClearHistory()
    }
   

   
    
    struct Theme{
        var searchBarBackgroundColor : CGColor  = .clear
        var searchBarTextColor : NSColor = .white
        var searchBarFont : NSFont = NSFont.systemFont(ofSize: 20, weight: .medium)
        var scrollViewBackgroundColor : NSColor = .controlBackgroundColor
        var tableViewBackgroundColor : NSColor = .clear
        var viewBackgroundColor : CGColor = .clear
        
    }
    

}

extension SearchTableViewController: NSTableViewDataSource {
    
   
  func numberOfRows(in tableView: NSTableView) -> Int {
      return dataForTable.count
  }
   
  func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        let pasteboardItem = NSPasteboardItem()
        if(dataForTable[row].sectionType != .sectionHeader){
            pasteboardItem.setString("\(row)", forType: pasteBoard)
        }
        return pasteboardItem
  }

    
    
  func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
      
      guard
          let item = info.draggingPasteboard.pasteboardItems?.first,
          let originalRow = Int(item.string(forType: pasteBoard)!)
          else { return [] }
      
      if dataForTable[originalRow].sectionType == .sectionRow &&
            dataForTable[row].sectionType == .sectionRow{
          let searchModelOfOriginalRow = dataForTable[originalRow].data as! Search
          let searchModelOfNewRow = dataForTable[row].data as! Search
          if dropOperation == .on && searchModelOfOriginalRow.searchType == searchModelOfNewRow.searchType{
                    return .move
                } else {
                    return []
                }
      }
      
      return []
    }
    
    
    
    
    
    
       
  func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
            guard
                let item = info.draggingPasteboard.pasteboardItems?.first,
                let originalRow = Int(item.string(forType: pasteBoard)!)
                else { return false }
            let newRow = row
            //print(originalRow,newRow)
            ////print(dataForTable[originalRow],dataForTable[newRow])
            let temp = dataForTable[originalRow]
            dataForTable.remove(at: originalRow)
            dataForTable.insert(temp, at: newRow)
            tableView.beginUpdates()
            tableView.moveRow(at: originalRow, to: newRow)
            tableView.endUpdates()
            return true
    }
    
    
   
}

extension SearchTableViewController: NSTableViewDelegate {
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat{
        let dataForRow =  dataForTable[row]
        if(dataForRow.sectionType == .sectionHeader){
            let searchType = dataForRow.data as! SearchType
            let searchSuggestionTableHeaderView = cachedTableViewCells[SearchSuggestionTableSectionHeaderViewCell.cellIdentifier] as! SearchSuggestionTableSectionHeaderViewCell
            searchSuggestionTableHeaderView.configureData(searchType: searchType.rawValue,index: row)
            searchSuggestionTableHeaderView.needsLayout = true
            searchSuggestionTableHeaderView.layout()
            searchSuggestionTableHeaderView.layoutSubtreeIfNeeded()
            //print(searchSuggestionTableHeaderView.contentView.bounds.height)
            tableHeight += searchSuggestionTableHeaderView.contentView.bounds.height + tableView.intercellSpacing.height
            return searchSuggestionTableHeaderView.contentView.bounds.height
        }
        else{
            let Search = dataForRow.data as! Search
            let search = cachedTableViewCells[SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier] as! SearchSuggestionAndSearchHistoryTableViewCell
            search.configureContent(seacrhSuggesionCellContents: Search)
            search.needsLayout = true
            search.layout()
            search.layoutSubtreeIfNeeded()
            //print(search.contentView.bounds.height)
            tableHeight += search.contentView.bounds.height + tableView.intercellSpacing.height
            return search.contentView.bounds.height
        }
        
        
        
    }
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        if(dataForTable[row].sectionType == .sectionHeader){
            return false
        }
        else{
            return true
        }
    }
    
  
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        //print("selected : " ,  tableView.selectedRow)
        
    }
   
    
    
  func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
      
      
      let dataForRow =  dataForTable[row]
      

      if(dataForRow.sectionType == .sectionHeader){
          let searchType = dataForRow.data as! SearchType
          if let searchSuggestionTableHeaderView = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(SearchSuggestionTableSectionHeaderViewCell.cellIdentifier), owner: self) as? SearchSuggestionTableSectionHeaderViewCell{
              searchSuggestionTableHeaderView.configureData(searchType: searchType.rawValue,index: row)
              searchSuggestionTableHeaderView.delegate = self
              return searchSuggestionTableHeaderView
          }
          else{
              let searchSuggestionTableHeaderView = SearchSuggestionTableSectionHeaderViewCell()
              searchSuggestionTableHeaderView.identifier = NSUserInterfaceItemIdentifier(SearchSuggestionTableSectionHeaderViewCell.cellIdentifier)
              searchSuggestionTableHeaderView.configureData(searchType: searchType.rawValue,index: row)
              searchSuggestionTableHeaderView.delegate = self
              
              return searchSuggestionTableHeaderView
          }
      }
      else{
          let Search = dataForRow.data as! Search
          if let search =  tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier), owner: self) as? SearchSuggestionAndSearchHistoryTableViewCell{
              search.configureContent(seacrhSuggesionCellContents: Search)
              return search
          }
          else{
              let search = SearchSuggestionAndSearchHistoryTableViewCell()
              search.identifier = NSUserInterfaceItemIdentifier(SearchSuggestionAndSearchHistoryTableViewCell.cellIdentifier)
              search.configureContent(seacrhSuggesionCellContents: Search)
              return search
          }
         
         
          
          
      }
      
      

  }
    
    func tableView(_ tableView: NSTableView, rowViewForRow row: Int) -> NSTableRowView? {
        if(dataForTable[row].sectionType == .sectionHeader){
            return NSTableRowView()
        }
        return SearchSuggestionTableRowView()
    }
    
    
  

    
   
    
    func tableView(_ tableView: NSTableView, rowActionsForRow row: Int, edge: NSTableView.RowActionEdge) -> [NSTableViewRowAction] {
        if(dataForTable[row].sectionType == .sectionRow ){
            if edge == .trailing {
                let deleteAction = NSTableViewRowAction(style: .destructive, title: "Delete", handler: {
                    [weak self] (nil, row)  in
                    self?.dataForTable.remove(at: row)
                    tableView.removeRows(at: IndexSet(integer: row), withAnimation: .effectFade)
                })
                deleteAction.backgroundColor = NSColor.red
               
                    deleteAction.image = NSImage(systemSymbolName: "xmark.bin.fill", accessibilityDescription: "Delete")?.withSymbolConfiguration(.init(scale: .large))?.image(withTintColor: .white.withAlphaComponent(0.9))
                
                return [deleteAction]
            } else {
                let addToSearchBarAction = NSTableViewRowAction(style: .regular, title: "Add To Search Bar", handler: {[weak self]_,_ in
                    let  dataForRow = self?.dataForTable[row].data as! Search
                    self?.searchBar.stringValue = dataForRow.searchItemName
                })
                addToSearchBarAction.backgroundColor = NSColor.systemBlue
               
                    addToSearchBarAction.image = NSImage(systemSymbolName: "arrow.up", accessibilityDescription: "select To Top")?.withSymbolConfiguration(.init(scale: .large))?.image(withTintColor: .white.withAlphaComponent(0.9))
                
                
                return [addToSearchBarAction]
            }
            
           
        }
        else{
            return []
        }
    }
    
    func tableViewColumnDidResize(_ notification: Notification) {
        tableView.floatsGroupRows = false
        tableHeight = 0
            let allIndexes = IndexSet(integersIn: 0..<tableView.numberOfRows)
            tableView.noteHeightOfRows(withIndexesChanged: allIndexes)
        //print(tableHeight,tableView.frame.height)
        tableView.frame.size.height = tableHeight
        tableView.floatsGroupRows = true
    }
    
    
    
    
    
    func tableView(_ tableView: NSTableView, isGroupRow row: Int) -> Bool {
        if(dataForTable[row].sectionType == .sectionHeader){
            return true
        }
        else{
            return false
        }
    }

    
    func popoverShouldDetach(_ popover: NSPopover) -> Bool {
        return true
    }
    
  
    
    
    
    
}

enum TableViewSectionType {
    case sectionHeader
    case sectionRow
}


protocol SearchTableViewControllerContract : NSViewController{
    var presenter : SearchTablePresenterContract? {get set}
    func updatePincodeDetailsInSearchBar(PincodeDetails : PincodeDetails)
}

