//
//  GetListOfRestaurantDetailsUseCase.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation


class getRestaurantsDetailsRequest : Request{
   
    var locality : String
    var pincode : String
     init(locality: String, pincode: String,requestType: RequestType) {
        self.locality = locality
        self.pincode = pincode
        super.init(requestType: requestType)
    }
}



class getRestaurantsDetailsResponse : Response{
    
    
    var listOfRestaurantDetails : [RestaurantCompleteDetails]
    init(listOfRestaurantDetails: [RestaurantCompleteDetails],requestType: RequestType, responseStatus: ResponseStatus) {
       self.listOfRestaurantDetails = listOfRestaurantDetails
       super.init(requestType: requestType, responseStatus: responseStatus)
   }
}


class GetListOfRestaurantDetails : UseCase<getRestaurantsDetailsRequest,getRestaurantsDetailsResponse> {
   
    
    private let dataManager : RestaurantDetailsDataManagerContract?
    
    init(dataManager :RestaurantDetailsDataManagerContract){
        self.dataManager  = dataManager
    }
    
    override func run(request: getRestaurantsDetailsRequest, callback: @escaping (getRestaurantsDetailsResponse) -> Void) {
        
        let parameter = getRestaurantsDetailsParameter(locality: request.locality, pincode: request.pincode, requestType: request.requestType)
        dataManager?.getListOfRestaurentDetails(withParameter: parameter, success: { listOfRestaurantDetails in
            let response  = getRestaurantsDetailsResponse(listOfRestaurantDetails: listOfRestaurantDetails ?? [], requestType: request.requestType, responseStatus: .success)
            self.invoke(callback: callback, response: response)
        }, failure: { responseStatus in
            let response  = getRestaurantsDetailsResponse(listOfRestaurantDetails: [], requestType: request.requestType, responseStatus: responseStatus)
            self.invoke(callback: callback, response: response)
        })
    }
    
   
    
}


