//
//  GetPincodeDetails.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 24/06/22.
//

import Foundation

class GetPincodeDetails : UseCase<GetPincodeDetailsRequest , GetPincodeDetailsResponse>{
   
    
    private let dataManager : PincodeDetailsDataManagerContract?
    
    init(dataManager :PincodeDetailsDataManagerContract){
        self.dataManager  = dataManager
    }
    
    override func run(request: GetPincodeDetailsRequest, callback: @escaping (GetPincodeDetailsResponse) -> Void) {
        let parameter = GetPincodeDetailsParameter(pincode: request.pincode, requestType: request.requestType)
        dataManager?.GetPincodeDetails(withParameter: parameter, success: { pincodeDetails in
            let response = GetPincodeDetailsResponse(pincodeDetails: pincodeDetails, requestType: request.requestType, responseStatus: .success)
            self.invoke(callback: callback, response: response)
        }, failure: { responseStatus in
            let response = GetPincodeDetailsResponse(pincodeDetails: PincodeDetails(), requestType: request.requestType, responseStatus: responseStatus)
            self.invoke(callback: callback, response: response)
        })
    }
    
    
    
}

class GetPincodeDetailsRequest : Request{
  
    var pincode : String
    
    init(pincode : String,requestType: RequestType) {
        self.pincode = pincode
        super.init(requestType: requestType)
    }
}


class GetPincodeDetailsResponse : Response{
    var pincodeDetails :  PincodeDetails
    
    init(pincodeDetails :  PincodeDetails,requestType: RequestType, responseStatus: ResponseStatus) {
        self.pincodeDetails = pincodeDetails
        super.init(requestType: requestType, responseStatus: responseStatus)
    }
}


