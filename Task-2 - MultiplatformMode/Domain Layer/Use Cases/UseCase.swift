//
//  UseCase.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 27/06/22.
//

import Foundation

enum RequestType {
    case local
    case server
    case both
    case refresh
    case serverIfNotlocal
}

enum ResponseStatus {
    case success
    case networkUnavailable
    case timeout
    case authenticationFailure
    case unknownError
    case databaseFailure
    case parserFailure
}

class Request {
   
    
     var requestType: RequestType
    
     init(requestType: RequestType) {
        self.requestType = requestType
    }
  
}


class Response {
    
    
     var requestType: RequestType
     var responseStatus: ResponseStatus
    
    init(requestType: RequestType, responseStatus: ResponseStatus) {
        self.requestType = requestType
        self.responseStatus = responseStatus
    }
    
}


class UseCase<A : Request, B : Response> : NSObject{
    
     func execute(request: A, callback: @escaping (_ response: B) -> Void = { _ in }) {
        DispatchQueue.global().async { [weak self] in
            self?.run(request: request, callback: callback)
        }
    }
    
     func run(request: A, callback: @escaping (_ response: B) -> Void) {
        
    }
    
     func invoke(callback: @escaping (_ response: B) -> Void, response: B) {
        if Thread.isMainThread {
            callback(response)
        } else {
            DispatchQueue.main.async(execute: {
                callback(response)
            })
        }
    }
}
