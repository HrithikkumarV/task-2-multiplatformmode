//
//  DataManagerProtolols.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation

protocol DataManagerContracts : RestaurantDetailsDataManagerContract,PincodeDetailsDataManagerContract{
    
}

protocol RestaurantDetailsDataManagerContract{
    func getListOfRestaurentDetails(withParameter: getRestaurantsDetailsParameter, success: ( _ listOfRestaurantDetails :[RestaurantCompleteDetails]?) -> Void, failure : @escaping (_ responseStatus : ResponseStatus) -> Void)
}


protocol PincodeDetailsDataManagerContract{
    func GetPincodeDetails(withParameter: GetPincodeDetailsParameter, success: @escaping ( _ pincodeDetails :PincodeDetails) -> Void, failure : @escaping (_ responseStatus : ResponseStatus) -> Void)
}
