//
//  Parameters.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 27/06/22.
//

import Foundation

class Parameters {
   
    var requestType : RequestType
    init(requestType : RequestType) {
        self.requestType = requestType
    }
}

class getRestaurantsDetailsParameter : Parameters{
   
    var locality : String
    var pincode : String
     init(locality: String, pincode: String,requestType: RequestType) {
        self.locality = locality
        self.pincode = pincode
        super.init(requestType: requestType)
    }
}

class GetPincodeDetailsParameter : Parameters{
  
    var pincode : String
    
    init(pincode : String,requestType: RequestType) {
        self.pincode = pincode
        super.init(requestType: requestType)
    }
}
