//
//  File.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation
struct PincodeDetails{
    
    
    var country : String = ""
    var state : String = ""
    var city : String = ""
    var pincode : String = ""
    var localities : [String] = []
    
   
}
