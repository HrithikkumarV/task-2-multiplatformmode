//
//  RestaurantEntity.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation

struct Restaurant{
    
    
    var restaurantProfileImage : Data = Data()
     var restaurantName : String = ""
     var restaurantDescription : String = ""
     var restaurantCuisine : String = ""
     var restaurantContactNumber : String = ""
    
   
}
