//
//  AddressEntity.swift
//  Task - 1
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation
struct Address : Codable{
    
    
    
    var doorNoAndBuildingNameAndBuildingNo : String = ""
    var streetName : String = ""
    var localityName : String = ""
    var landmark : String = ""
    var city : String = ""
    var state : String = ""
    var pincode : String = ""
    var country : String = ""
    
   

}
