//
//  CouponCodeEntity.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 23/06/22.
//

import Foundation
struct CouponCode : Codable{
    let couponCode : String
    let discountPercentage , maximumDiscountAmount  : Int
}
