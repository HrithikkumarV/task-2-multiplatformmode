//
//  SearchUseCase.swift
//  Task-2 - MultiplatformMode
//
//  Created by Hrithik Kumar V on 22/06/22.
//

import Foundation


struct Search {
   
    
    var searchItemName : String
    var searchItemType : SearchItemType
    var searchType : SearchType
    
    init(searchItemName: String, searchItemType: SearchItemType, searchType: SearchType) {
        self.searchItemName = searchItemName
        self.searchItemType = searchItemType
        self.searchType = searchType
    }
}

enum SearchType : String{
    case searchHistoryItem = "Search History Item"
    case searchSuggestionItem = "Search suggestion Item"
}

enum SearchItemType : String{
    case Menu
    case Restaurant
    case Search
}
